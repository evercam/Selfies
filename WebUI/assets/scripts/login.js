var Login = function () {
    
    var createUserState = false;
    var exitRegister = false;
    var EvercamApi = "https://api.evercam.io/v1";
    var utilsApi = "http://apiutils.azurewebsites.net/v1";

    var onBodyLoad = function() {
        if (localStorage.getItem("oAuthToken") != null && localStorage.getItem("oAuthToken") != undefined)
            window.location = 'index.html';
        $("#country").select2({
            placeholder: '<i class="icon-map-marker"></i>&nbsp;Select a Country',
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function(m) {
                return m;
            }
        });
    };

    var format = function(state) {
        if (!state.id) return state.text;
        return "<img class='flag' src='assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
    };

    var getParameterByName = function(name, searchString) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(searchString);
        return results == null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    var handleLogin = function() {

        if (localStorage.getItem("oAuthToken") != null && localStorage.getItem("oAuthToken") != undefined) {
            window.location = 'index.html';
        } else {
            var stringHash = window.location.hash;
            if (stringHash != "") {
                var hasToken = getParameterByName('access_token', "?" + stringHash.substring(1));
                if (hasToken != "" && hasToken != null) {
                    $(".login-form").hide();
                    $("#divLoggingIn").show();
                    var hasTokenType = getParameterByName('token_type', window.location.hash);
                    var tokenExpiry = getParameterByName('expires_in', window.location.hash);
                    $.ajax({
                        type: 'POST',
                        url: utilsApi + '/tokeninfo',
                        dataType: 'json',
                        data: { token_endpoint: "https://api.evercam.io/oauth2/tokeninfo?access_token=" + hasToken },
                        ContentType: 'application/x-www-form-urlencoded',
                        success: function(res) {
                            if (res.userid != "null" && res.userid != '') {
                                var userId = res.userid;
                                localStorage.setItem("oAuthTokenType", hasTokenType);
                                localStorage.setItem("oAuthToken", hasToken);
                                localStorage.setItem("tokenExpiry", tokenExpiry);

                                localStorage.setItem("evercamUserId", userId);
                                localStorage.setItem("evercamUsername", userId);
                                getUsersInfo();
                            }
                        },
                        error: function(xhr, textStatus) {

                        }
                    });
                }
            }
        }
    };

    var getUsersInfo = function() {
        $.ajax({
            type: "GET",
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            url: EvercamApi + "/users/" + localStorage.getItem("evercamUserId") + ".json",
            beforeSend: function(xhrObj) {
                xhrObj.setRequestHeader("Authorization", localStorage.getItem("oAuthTokenType") + " " + localStorage.getItem("oAuthToken"));
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(res) {
                loggedInUser = res.users[0];
                if (res.users[0].forename == "" && res.users[0].lastname == "")
                    return;
                localStorage.setItem("evercamUsername", res.users[0].forename + " " + res.users[0].lastname);
                setTimeout(function() { window.location = 'index.html'; }, 300);
            },
            error: function(xhrc, ajaxOptionsc, thrownErrorc) { /* window.location = 'Cameras.html'; */
            }
        });
    };

    var clearForm = function() {
        $("#first_name").val("");
        $("#last_name").val("");
        $("#user_name").val("");
        $("#user_email").val("");
        $("#country").val("");
        $('.alert-error').slideUp();
        $('#LoaderRegister').hide();
        $(".font-size16").removeClass("font-color-red");
    };

    var handleRegisterUser = function() {
        $(".register_user").bind("click", function() {
            if (exitRegister) return;
            if (!createUserState) {
                createUserState = true;
                $("#divRegister").slideDown(900, function() {
                    $("#spnCamcel").fadeIn();
                });
            } else {
                if ($("#first_name").val() == "" && $("#last_name").val() == "" && $("#user_name").val() == "" && $("#user_email").val() == "" && $("#password").val() == "" && $("#country").val() == "") {
                    $('.alert-error').slideDown();
                    $('.alert-error span').html('Please enter required fields.');
                    $(".font-size16").addClass("font-color-red");
                    return;
                } else {
                    $(".font-size16").removeClass("font-color-red");
                    var isReturn = false;

                    if ($("#first_name").val() == "") {
                        $('.alert-error').slideDown();
                        $('.alert-error span').html('Please enter required field: First Name.');
                        $("#spnReqFN").addClass("font-color-red");
                        isReturn = true;
                    }
                    if ($("#last_name").val() == "") {
                        $('.alert-error').slideDown();
                        $('.alert-error span').html('Please enter required field: Last Name.');
                        $("#spnReqLN").addClass("font-color-red");
                        isReturn = true;
                    }
                    if ($("#user_name").val() == "") {
                        $('.alert-error').slideDown();
                        $('.alert-error span').html('Please enter required field: Username.');
                        $("#spnReqUN").addClass("font-color-red");
                        isReturn = true;
                    }
                    if ($("#user_email").val() == "") {
                        $('.alert-error').slideDown();
                        $('.alert-error span').html('Please enter required field: Email.');
                        $("#spnReqEmail").addClass("font-color-red");
                        isReturn = true;
                    }
                    if ($("#password").val() == "") {
                        $('.alert-error').slideDown();
                        $('.alert-error span').html('Please enter required field: Password.');
                        $("#spnReqPass").addClass("font-color-red");
                        isReturn = true;
                    }
                    if ($("#country").val() == "") {
                        $('.alert-error').slideDown();
                        $('.alert-error span').html('Please enter required field: Country.');
                        $("#spnReqCountry").addClass("font-color-red");
                        isReturn = true;
                    }
                    if (isReturn) {
                        $('.alert-error').slideDown();
                        $('.alert-error span').html('Please enter required fields.');
                        return;
                    }

                    $(".font-size16").removeClass("font-color-red");
                    $('.alert-error').slideUp();
                    $('#LoaderRegister').css({
                        position: 'absolute',
                        top: ($('#divRegister').height() / 2) - 22,
                        'z-index': '5',
                        left: ($('#divRegister').width() / 2) - 22,
                    });
                    $('#LoaderRegister').show();

                    $.ajax({
                        type: 'POST',
                        crossDomain: true,
                        url: EvercamApi + '/users.json',
                        data: { firstname: $("#first_name").val(), lastname: $("#last_name").val(), username: $("#user_name").val(), country: $("#country").val().toLowerCase(), email: $("#user_email").val(), password: $("#password").val() },
                        dataType: 'json',
                        ContentType: 'application/json; charset=utf-8',
                        success: function(res) {
                            $('.alert-error').slideUp();
                            createUserState = false;
                            exitRegister = true;
                            $("#spnCamcel").fadeOut();
                            $('.register_user img').attr('src', 'assets/img/ecupg.png');
                            $("#divRegister").slideUp(900, function() {
                                $("#divSuccess").html('Thank you for registering, <b>' + res.users[0].username + '</b>. An email has been dispatched to <b>' + res.users[0].email + '</b> with details on how to activate your account.');
                                $("#divSuccess").fadeIn();
                                clearForm();
                            });
                        },
                        error: function(xhr, textStatus) {
                            //var msg = '<ul>';
                            //for (var i = 0; i < xhr.responseJSON.message.length; i++)
                            //    msg += '<li>' + xhr.responseJSON.message[i] + '</li>';
                            $('.alert-error').slideDown();
                            $('.alert-error span').html(xhr.responseJSON.message + ' ' + xhr.responseJSON.context);
                            $('#LoaderRegister').hide();
                        }
                    });
                }
            }
        });

        $(".cancel_register").bind("click", function() {
            createUserState = false;
            $("#spnCamcel").fadeOut();
            $("#divRegister").slideUp(900, function() {
                clearForm();
            });
        });
    };
    
    return {
        //main function to initiate the module
        init: function () {
            onBodyLoad();
            handleRegisterUser();
            handleLogin();
        }

    };

}();