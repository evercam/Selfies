﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Selfies.Entities;
using System.IO;

namespace Selfies.Dao
{
    public class SelfiesTweetDao
    {
        public static SelfiesTweet GetSelfiesTweetByHash(string HashTag, string twitterId, string evercamId)
        {
            SelfiesTweet selfiestweet = new SelfiesTweet();
            try
            {
                const string sql = @"select * from Selfies where HashTag=@HashTag and TwitterId=@TwitterId and EvercamId=@evercamId and IsDeleted=0";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@HashTag", string.IsNullOrEmpty(HashTag) ? (object)DBNull.Value : HashTag);
                cmd.Parameters.AddWithValue("@TwitterId", string.IsNullOrEmpty(twitterId) ? (object)DBNull.Value : twitterId);
                cmd.Parameters.AddWithValue("@evercamId", string.IsNullOrEmpty(evercamId) ? (object)DBNull.Value : evercamId);
                
                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (!dr.IsDBNull(dr.GetOrdinal("Id")))
                        selfiestweet.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    if (!dr.IsDBNull(dr.GetOrdinal("TwitterId")))
                        selfiestweet.TwitterId = dr["TwitterId"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("EvercamId")))
                        selfiestweet.EvercamId = dr["EvercamId"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("CameraId")))
                        selfiestweet.CameraId = dr["CameraId"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("PhotoText")))
                        selfiestweet.PhotoText = dr["PhotoText"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("HashTag")))
                        selfiestweet.HashTag = dr["HashTag"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("SnapshotUrl")))
                        selfiestweet.SnapshotUrl = dr["SnapshotUrl"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("Username")))
                        selfiestweet.Username = dr["Username"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("Password")))
                        selfiestweet.Password = dr["Password"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("CreatedAt")))
                        selfiestweet.CreatedAt = dr.GetDateTime(dr.GetOrdinal("CreatedAt"));

                    if (!dr.IsDBNull(dr.GetOrdinal("IsActive")))
                        selfiestweet.IsActive = dr.GetBoolean(dr.GetOrdinal("IsActive"));
                    if (!dr.IsDBNull(dr.GetOrdinal("LogoFile")))
                        selfiestweet.LogoFile = dr["LogoFile"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("IsDeleted")))
                        selfiestweet.IsDeleted = dr.GetBoolean(dr.GetOrdinal("IsDeleted"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Response")))
                        selfiestweet.Response = dr["Response"].ToString();
                }
                dr.Close();
                dr.Dispose();
                cmd.Dispose();
                DbConnection.CloseConnection();
                return selfiestweet;
            }
            catch (Exception ex)
            {
                string msg =
                    "SelfiesTweetDao GetSelfiesTweetByHash(string HashTag, string twitterId, string evercamId) <br /> Log Message: " + ex.Message;
                LogError(msg);
                return selfiestweet;
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static List<SelfiesTweet> GetAllSelfiesTweet(string evercamId)
        {
            List<SelfiesTweet> selfiestweets = new List<SelfiesTweet>();
            try
            {
                const string sql = @"select s.* from Selfies s where s.EvercamId=@EvercamId and s.IsDeleted=0 order by Id desc";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@EvercamId", string.IsNullOrEmpty(evercamId) ? (object)DBNull.Value : evercamId);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    SelfiesTweet selfiestweet = new SelfiesTweet();
                    if (!dr.IsDBNull(dr.GetOrdinal("Id")))
                        selfiestweet.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    if (!dr.IsDBNull(dr.GetOrdinal("TwitterId")))
                        selfiestweet.TwitterId = dr["TwitterId"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("EvercamId")))
                        selfiestweet.EvercamId = dr["EvercamId"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("AccessToken")))
                        selfiestweet.AccessToken = dr["AccessToken"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("CameraId")))
                        selfiestweet.CameraId = dr["CameraId"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("PhotoText")))
                        selfiestweet.PhotoText = dr["PhotoText"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("HashTag")))
                        selfiestweet.HashTag = dr["HashTag"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("SnapshotUrl")))
                        selfiestweet.SnapshotUrl = dr["SnapshotUrl"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("Username")))
                        selfiestweet.Username = dr["Username"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("Password")))
                        selfiestweet.Password = dr["Password"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("CreatedAt")))
                        selfiestweet.CreatedAt = dr.GetDateTime(dr.GetOrdinal("CreatedAt"));
                    if (!dr.IsDBNull(dr.GetOrdinal("IsActive")))
                        selfiestweet.IsActive = dr.GetBoolean(dr.GetOrdinal("IsActive"));
                    if (!dr.IsDBNull(dr.GetOrdinal("LogoFile")))
                        selfiestweet.LogoFile = dr["LogoFile"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("IsDeleted")))
                        selfiestweet.IsDeleted = dr.GetBoolean(dr.GetOrdinal("IsDeleted"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Response")))
                        selfiestweet.Response = dr["Response"].ToString();
                    selfiestweets.Add(selfiestweet);
                }
                dr.Close();
                dr.Dispose();
                cmd.Dispose();
                DbConnection.CloseConnection();
                return selfiestweets;
            }
            catch (Exception ex)
            {
                string msg =
                    "SelfiesTweetDao GetAllSelfiesTweet() <br /> Log Message: " + ex.Message;
                LogError(msg);
                return selfiestweets;
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static bool InsertSelfiesTweet(SelfiesTweet selfiestweet)
        {
            bool result = false;
            try
            {
                const string sql = @"insert into Selfies(TwitterId, CameraId, PhotoText, HashTag, SnapshotUrl, Username, Password,IsActive,LogoFile,Response,EvercamId,AccessToken) 
                                    values (@TwitterId, @CameraId, @PhotoText, @HashTag, @SnapshotUrl, @Username, @Password,@IsActive,@LogoFile,@Response,@EvercamId,@AccessToken) 
                                    SELECT Id FROM Selfies WHERE Id=Scope_Identity()";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@TwitterId", selfiestweet.TwitterId);
                cmd.Parameters.AddWithValue("@CameraId", string.IsNullOrEmpty(selfiestweet.CameraId) ? (object)DBNull.Value : selfiestweet.CameraId);
                cmd.Parameters.AddWithValue("@PhotoText", string.IsNullOrEmpty(selfiestweet.PhotoText) ? (object)DBNull.Value : selfiestweet.PhotoText);
                cmd.Parameters.AddWithValue("@HashTag", string.IsNullOrEmpty(selfiestweet.HashTag) ? (object)DBNull.Value : selfiestweet.HashTag.ToLower());
                cmd.Parameters.AddWithValue("@SnapshotUrl", string.IsNullOrEmpty(selfiestweet.SnapshotUrl) ? (object)DBNull.Value : selfiestweet.SnapshotUrl);
                cmd.Parameters.AddWithValue("@Username", string.IsNullOrEmpty(selfiestweet.Username) ? (object)DBNull.Value : selfiestweet.Username);
                cmd.Parameters.AddWithValue("@Password", string.IsNullOrEmpty(selfiestweet.Password) ? (object)DBNull.Value : selfiestweet.Password);
                cmd.Parameters.AddWithValue("@IsActive", selfiestweet.IsActive);
                cmd.Parameters.AddWithValue("@LogoFile", string.IsNullOrEmpty(selfiestweet.LogoFile) ? (object)DBNull.Value : selfiestweet.LogoFile);
                cmd.Parameters.AddWithValue("@Response", string.IsNullOrEmpty(selfiestweet.Response) ? (object)DBNull.Value : selfiestweet.Response);
                cmd.Parameters.AddWithValue("@EvercamId", string.IsNullOrEmpty(selfiestweet.EvercamId) ? (object)DBNull.Value : selfiestweet.EvercamId);
                cmd.Parameters.AddWithValue("@AccessToken", string.IsNullOrEmpty(selfiestweet.AccessToken) ? (object)DBNull.Value : selfiestweet.AccessToken);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                SqlDataReader dr = cmd.ExecuteReader();//try to update the object
                while (dr.Read())
                {
                    if (!dr.IsDBNull(dr.GetOrdinal("Id")))
                        selfiestweet.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                }
                dr.Close();
                dr.Dispose();
                cmd.Dispose();
                DbConnection.CloseConnection();
                result = true;
            }
            catch (Exception ex)
            {
                string msg =
                    "SelfiesTweetDao InsertSelfiesTweet(SelfiesTweet selfiestweet) <br /> Log Message: " + ex.Message;
                LogError(msg);
            }
            finally
            { DbConnection.CloseConnection(); }
            return result;
        }

        public static bool UpdateSelfiesTweet(SelfiesTweet selfiestweet)
        {
            bool result = false;
            try
            {
                const string sql = @"update Selfies set PhotoText=@PhotoText, SnapshotUrl=@SnapshotUrl, Username=@Username, Password=@Password, LogoFile=@LogoFile, Response=@Response, AccessToken=@AccessToken 
                                    where HashTag=@HashTag and TwitterId=@TwitterId and EvercamId=@evercamId and IsDeleted=0";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@TwitterId", selfiestweet.TwitterId);
                cmd.Parameters.AddWithValue("@PhotoText", string.IsNullOrEmpty(selfiestweet.PhotoText) ? (object)DBNull.Value : selfiestweet.PhotoText);
                cmd.Parameters.AddWithValue("@HashTag", string.IsNullOrEmpty(selfiestweet.HashTag) ? (object)DBNull.Value : selfiestweet.HashTag.ToLower());
                cmd.Parameters.AddWithValue("@SnapshotUrl", string.IsNullOrEmpty(selfiestweet.SnapshotUrl) ? (object)DBNull.Value : selfiestweet.SnapshotUrl);
                cmd.Parameters.AddWithValue("@Username", string.IsNullOrEmpty(selfiestweet.Username) ? (object)DBNull.Value : selfiestweet.Username);
                cmd.Parameters.AddWithValue("@Password", string.IsNullOrEmpty(selfiestweet.Password) ? (object)DBNull.Value : selfiestweet.Password);
                cmd.Parameters.AddWithValue("@IsActive", selfiestweet.IsActive);
                cmd.Parameters.AddWithValue("@LogoFile", string.IsNullOrEmpty(selfiestweet.LogoFile) ? (object)DBNull.Value : selfiestweet.LogoFile);
                cmd.Parameters.AddWithValue("@Response", string.IsNullOrEmpty(selfiestweet.Response) ? (object)DBNull.Value : selfiestweet.Response);
                cmd.Parameters.AddWithValue("@EvercamId", string.IsNullOrEmpty(selfiestweet.EvercamId) ? (object)DBNull.Value : selfiestweet.EvercamId);
                cmd.Parameters.AddWithValue("@AccessToken", string.IsNullOrEmpty(selfiestweet.AccessToken) ? (object)DBNull.Value : selfiestweet.AccessToken);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                DbConnection.CloseConnection();
                result = true;
            }
            catch (Exception ex)
            {
                string msg =
                    "SelfiesTweetDao UpdateSelfiesTweet(SelfiesTweet selfiestweet) <br /> Log Message: " + ex.Message;
                LogError(msg);
            }
            finally
            { DbConnection.CloseConnection(); }
            return result;
        }

        public static bool UpdateSelfiesTweetById(SelfiesTweet selfiestweet)
        {
            bool result = false;
            try
            {
                const string sql = @"update Selfies set PhotoText=@PhotoText, SnapshotUrl=@SnapshotUrl, Username=@Username, Password=@Password,CameraId=@CameraId, LogoFile=@LogoFile, 
                                    HashTag=@HashTag, Response=@Response, AccessToken=@AccessToken where Id=@Id and IsDeleted=0";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@Id", selfiestweet.Id);
                cmd.Parameters.AddWithValue("@TwitterId", selfiestweet.TwitterId);
                cmd.Parameters.AddWithValue("@PhotoText", string.IsNullOrEmpty(selfiestweet.PhotoText) ? (object)DBNull.Value : selfiestweet.PhotoText);
                cmd.Parameters.AddWithValue("@HashTag", string.IsNullOrEmpty(selfiestweet.HashTag) ? (object)DBNull.Value : selfiestweet.HashTag.ToLower());
                cmd.Parameters.AddWithValue("@SnapshotUrl", string.IsNullOrEmpty(selfiestweet.SnapshotUrl) ? (object)DBNull.Value : selfiestweet.SnapshotUrl);
                cmd.Parameters.AddWithValue("@Username", string.IsNullOrEmpty(selfiestweet.Username) ? (object)DBNull.Value : selfiestweet.Username);
                cmd.Parameters.AddWithValue("@Password", string.IsNullOrEmpty(selfiestweet.Password) ? (object)DBNull.Value : selfiestweet.Password);
                cmd.Parameters.AddWithValue("@CameraId", string.IsNullOrEmpty(selfiestweet.CameraId) ? (object)DBNull.Value : selfiestweet.CameraId);
                cmd.Parameters.AddWithValue("@IsActive", selfiestweet.IsActive);
                cmd.Parameters.AddWithValue("@LogoFile", string.IsNullOrEmpty(selfiestweet.LogoFile) ? (object)DBNull.Value : selfiestweet.LogoFile);
                cmd.Parameters.AddWithValue("@Response", string.IsNullOrEmpty(selfiestweet.Response) ? (object)DBNull.Value : selfiestweet.Response);
                cmd.Parameters.AddWithValue("@AccessToken", string.IsNullOrEmpty(selfiestweet.AccessToken) ? (object)DBNull.Value : selfiestweet.AccessToken);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                DbConnection.CloseConnection();
                result = true;
            }
            catch (Exception ex)
            {
                string msg =
                    "SelfiesTweetDao UpdateSelfiesTweet(SelfiesTweet selfiestweet) <br /> Log Message: " + ex.Message;
                LogError(msg);
            }
            finally
            { DbConnection.CloseConnection(); }
            return result;
        }

        public static bool UpdateSelfiesTwiterId(string evercamId, string twitterId)
        {
            bool result = false;
            try
            {
                const string sql = @"update Selfies set TwitterId=@TwitterId where EvercamId=@EvercamId";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@EvercamId", evercamId);
                cmd.Parameters.AddWithValue("@TwitterId", string.IsNullOrEmpty(twitterId) ? (object)DBNull.Value : twitterId);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                DbConnection.CloseConnection();
                result = true;
            }
            catch (Exception ex)
            {
                string msg =
                    "SelfiesTweetDao UpdateSelfiesTwiterId(string evercamId, string twitterId) <br /> Log Message: " + ex.Message;
                LogError(msg);
            }
            finally
            { DbConnection.CloseConnection(); }
            return result;
        }

        public static void ActiveSelfies(int selfiesId, bool isActive)
        {
            try
            {
                const string sql = @"update Selfies set IsActive=@IsActive where Id=@Id and IsDeleted=0";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@Id", selfiesId);
                cmd.Parameters.AddWithValue("@IsActive", isActive);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                DbConnection.CloseConnection();
            }
            catch (Exception ex)
            {
                string msg =
                    "SelfiesTweetDao ActiveSelfies(int selfiesId, bool isActive) <br /> Log Message: " + ex.Message;
                LogError(msg);
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static void DeleteSelfies(int selfiestweetId)
        {
            try
            {
                const string sql = @"delete from Selfies where Id=@Id and IsDeleted=1";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@Id", selfiestweetId);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                DbConnection.CloseConnection();
            }
            catch (Exception ex)
            {
                string msg =
                    "SelfiesTweetDao DeleteSelfies(int selfiestweetId) <br /> Log Message: " + ex.Message;
                LogError(msg);
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static void DeleteSelfiesLogically(int selfiestweetId)
        {
            try
            {
                const string sql = @"update Selfies set IsDeleted=1 where Id=@Id";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@Id", selfiestweetId);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                DbConnection.CloseConnection();
            }
            catch (Exception ex)
            {
                string msg =
                    "SelfiesTweetDao DeleteSelfiesLogically(int selfiestweetId) <br /> Log Message: " + ex.Message;
                LogError(msg);
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static void UpdateUserToken(string user, string access_token)
        {
            try
            {
                const string sql = @"update Selfies set AccessToken=@AccessToken where EvercamId=@EvercamId";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@EvercamId", user);
                cmd.Parameters.AddWithValue("@AccessToken", string.IsNullOrEmpty(access_token) ? (object)DBNull.Value : access_token);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                DbConnection.CloseConnection();
            }
            catch (Exception ex)
            {
                string msg =
                    "SelfiesTweetDao UpdateUserToken(string user, string access_token) <br /> Log Message: " + ex.Message;
                LogError(msg);
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static void LogError(string message)
        {
            try
            {
                var fileName = @"C:\selfiesdemo\logs\SelfiesTweetDao.txt";
                StreamWriter file = new StreamWriter(fileName, true);
                file.WriteLine(message + "\t" + DateTime.UtcNow + "\n");
                file.Close();
            }
            catch (Exception) { }

        }
    }
}