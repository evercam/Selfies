﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Entities
{
    public class Snapshot
    {
        public string HashTag { get; set; }
        public byte[] Data { get; set; }
        public DateTime CreatedAt { get; set; }
    }

    public class Snapshots
    {
        List<Snapshot> snaps;
    }
}
