using System;
using System.Threading;
using System.Diagnostics;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using BLL;
using BLL.DAO;
using BLL.Entities;
using AppModule.InterProcessComm;
using AppModule.NamedPipes;

namespace Server {

	public sealed class ServerNamedPipe : IDisposable {
		internal ServerPipeConnection PipeConnection;
        internal Thread PipeThread;
        internal bool Listen = true;
		internal DateTime LastAction;
		private bool disposed = false;

		private void PipeListener() {
			CheckIfDisposed();

			try 
            {
				Listen = Form1.PipeManager.Listen;
				//AppendText("Pipe " + this.PipeConnection.NativeHandle.ToString() + ": new pipe started" + Environment.NewLine);
				while (Listen) {
					LastAction = DateTime.Now;
                    string response = "";
					string request = PipeConnection.Read();
					LastAction = DateTime.Now;
					if (request.Trim() != "") 
                    {
                        string json = request;
                        if (request.IndexOf("{") >= 0)
                            json = request.Substring(request.IndexOf("{")).Trim();
                        else if (request.IndexOf(":") >= 0)
                            json = request.Substring(request.IndexOf(":") + 1).Trim();
                        
                        string[] split = request.Split(new char[] { '@', ':' });
                        string tid = split[1];
                        string key = Utils.ProcessKey(tid);

                        Form1.ActivityRef.AppendText(request + Environment.NewLine);

                        if (!json.StartsWith("{") && json.ToLower().EndsWith("hi"))
                        {
                            response = BLL.Utils.ServerName + ": hello @" + tid;
                            Form1.ActivityRef.AppendText(response + Environment.NewLine);
                            PipeConnection.Write(response);
                        }
                        else if (!json.StartsWith("{") && json.StartsWith("Error#"))
                        {
                            if (Form1._processInfos.ContainsKey(key))
                            {
                                Form1.ListenerProcessInfo info = Form1._processInfos[key];

                                KillProcess(info.ProcessId);
                                Form1._processInfos.Remove(key);
                                Form1._Twitters.Remove(key);

                                Form1.ActivityRef.AppendText("KILL: @" + key + Environment.NewLine);
                                Utils.DBLog(LogTypes.Debug, "@" + key + " Listener Killed", key, "Server");

                                Thread.Sleep(5000);

                                Twitter twitter = TwitterDao.Get(tid);
                                if (!twitter.IsActive)
                                {
                                    Form1.ActivityRef.AppendText("DESTART: @" + key + Environment.NewLine);
                                    Utils.DBLog(LogTypes.Debug, "@" + key + " Listener is Disabled", key, "Server");
                                }
                                else if (Form1.StartListener(twitter).ProcessId > 0)
                                {
                                    Form1.ActivityRef.AppendText("RESTART: @" + key + Environment.NewLine);
                                    Utils.DBLog(LogTypes.Debug, "@" + key + " Listener Restarted", key, "Server");
                                }
                                else
                                {
                                    Form1.ActivityRef.AppendText("NOSTART: @" + key + Environment.NewLine);
                                    Utils.DBLog(LogTypes.Debug, "@" + key + " Listener Could not be Restarted", key, "Server");
                                }
                            }
                            else
                            {
                                Form1.ActivityRef.AppendText("NOPROCESS: @" + key + Environment.NewLine);
                                foreach (var i in Form1._processInfos.Keys)
                                    Form1.ActivityRef.AppendText("Process: " + i + ", " + Form1._processInfos[i].ProcessId + ", " + Form1._processInfos[i].IsResponding + ", " + Form1._processInfos[i].ProcessPath + Environment.NewLine);
                            }
                        }
                        else if (json.StartsWith("{"))
                        {
                            List<Selfies> list = CreateSelfies(tid, json);
                            response = "";
                            foreach (Selfies s in list)
                                response += s.TwitterID + s.HashTag + ", ";

                            Form1.ActivityRef.AppendText(Utils.ServerName + ": " + response + Environment.NewLine);
                            response = JsonConvert.SerializeObject(list);
                            PipeConnection.Write(response);
                        }
                        else
                        {
                            //// just show the request on log, don't respond
                            response = BLL.Utils.ServerName + ": " + json;
                            Form1.ActivityRef.AppendText(response + Environment.NewLine);
                        }
					}
					else {
						PipeConnection.Write("Error: bad request");
                        Form1.ActivityRef.AppendText("Error: bad request" + Environment.NewLine);
					}
					LastAction = DateTime.Now;
					PipeConnection.Disconnect();
					if (Listen) {
						//Form1.ActivityRef.AppendText("Pipe " + this.PipeConnection.NativeHandle.ToString() + ": listening" + Environment.NewLine);
						Connect();
					}
					Form1.PipeManager.WakeUp();
				}
			} 
			catch (System.Threading.ThreadAbortException x) {
                Form1.ActivityRef.AppendText("ERROR (ThreadAbortException): " + x.ToString() + Environment.NewLine);
            }
			catch (System.Threading.ThreadStateException x) {
                Form1.ActivityRef.AppendText("ERROR (ThreadStateException): " + x.ToString() + Environment.NewLine);
            }
			catch (Exception x) {
                Form1.ActivityRef.AppendText("ERROR: " + x.ToString() + Environment.NewLine);
			}
			finally {
				this.Close();
			}
		}

        internal List<Selfies> CreateSelfies(string twitterid , string json)
        {
            string track = "a";
            //// find/load distinct #cameratags present in this tweet
            List<Selfies> selfies = new List<Selfies>();
            try
            {
                track = "b";
                TweetStatus status = JsonConvert.DeserializeObject<TweetStatus>(json);
                selfies = FindInSelfies(status.Tags, status.Listener);
                track = "c";
                //// add twitter user's default camera to selfies if tweet does not contain any #cameratag
                if (selfies.Count == 0)
                {
                    Selfies self = new Selfies();

                    //// get default camera details of this @mention user
                    if (Form1._Selfies.ContainsKey(Utils.SelfiesKey(twitterid, "")))
                    {
                        track = "d";
                        self = Form1._Selfies[Utils.SelfiesKey(status.Listener, "")];    // load from cache
                        Form1.ActivityRef.AppendText(Utils.ServerName + ": Default Selfies (" + self.ID + ") loaded from Cache" + Environment.NewLine);
                    }
                    else
                    {
                        track = "e";
                        self = SelfiesDao.GetDefault(status.Listener);   // load from DB
                        if (!string.IsNullOrEmpty(SelfiesDao.LastError))
                            Form1.ActivityRef.AppendText("SelfiesDao.GetDefault(" + status.Listener + ") Error: " + SelfiesDao.LastError + Environment.NewLine);
                        if (self != null && !string.IsNullOrEmpty(self.TwitterID))
                        {
                            Form1._Selfies.Add(Utils.SelfiesKey(self), self);           // add this default to cache NOW
                            Form1.ActivityRef.AppendText(Utils.ServerName + ": Default Selfies (" + self.ID + ") loaded from DB" + Environment.NewLine);
                        }
                    }
                    track = "f";
                    //// if no default camera found then do nothing
                    if (self != null && self.ID > 0 && self.IsActive)
                        selfies.Add(self);
                    track = "g";
                }
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "CreateSelfies(" + track + ") [Error] " + x.ToString(), "Server");
                Form1.ActivityRef.AppendText("Error CreateSelfies() " + track + ": " + x.Message + Environment.NewLine);
                return new List<Selfies>();
            }
            return selfies;
        }

        internal List<Selfies> FindInSelfies(string[] hashtags, string twitterid)
        {
            List<Selfies> selfies = new List<Selfies>();

            try
            {
                foreach (var tag in hashtags)
                {
                    string key = Utils.SelfiesKey(twitterid, "#" + tag);
                    if (Form1._Selfies.Keys.Contains(key))
                        selfies.Add(Form1._Selfies[key]);    // load it from cache
                    else
                    {
                        Selfies self = SelfiesDao.Get(twitterid, "#" + tag);      // load it from DB
                        if (self.ID > 0)
                        {
                            selfies.Add(self);
                            Form1._Selfies.Add(key, self);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "FindInSelfies(" + hashtags.ToString() + ") [Error] " + x.ToString(), "Server");
            }

            return selfies;
        }

        internal void KillProcess(int id)
        {
            try
            {
                ProcessStartInfo start = new ProcessStartInfo();
                start.FileName = "taskkill.exe";
                start.Arguments = "/pid " + id + " /F";
                start.UseShellExecute = false;

                Process process = new Process();
                start.CreateNoWindow = true;
                process.StartInfo = start;
                process.Start();
                process.WaitForExit(500);
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "KillProcess(" + id + ") [Error] " + x.ToString(), "Server");
            }
        }

		internal void Connect() {
			CheckIfDisposed();
			PipeConnection.Connect();
		}

		internal void Close() {
			CheckIfDisposed();
			this.Listen = false;
			Form1.PipeManager.RemoveServerChannel(this.PipeConnection.NativeHandle);
			this.Dispose();
		}

		internal void Start() {
			CheckIfDisposed();
			PipeThread.Start();
		}

		private void CheckIfDisposed() {
			if(this.disposed) {
				throw new ObjectDisposedException("ServerNamedPipe");
			}
		}

		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing) {
            if (!this.disposed && PipeConnection != null)
            {
				PipeConnection.Dispose();
				if (PipeThread != null) {
					try {
						PipeThread.Abort();
					} 
					catch (System.Threading.ThreadAbortException ex) { }
					catch (System.Threading.ThreadStateException ex) { }
					catch (Exception ex) {
						// Log exception
					}
				}
			}
			disposed = true;         
		}

		~ServerNamedPipe() {
			Dispose(false);
		}

		internal ServerNamedPipe(string name, uint outBuffer, uint inBuffer, int maxReadBytes, bool secure) {
			PipeConnection = new ServerPipeConnection(name, outBuffer, inBuffer, maxReadBytes, secure);
			PipeThread = new Thread(new ThreadStart(PipeListener));
			PipeThread.IsBackground = true;
			PipeThread.Name = "Pipe Thread " + this.PipeConnection.NativeHandle.ToString();
			LastAction = DateTime.Now;
		}
	}
}