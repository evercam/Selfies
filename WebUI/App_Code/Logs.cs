﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Selfies.Entities
{
    public class Logs
    {
        public int ID { get; set; }
        public string CreatedBy { get; set; }
        public string TwitterId { get; set; }
        public int SelfiesId { get; set; }
        public string Tweet { get; set; }
        public ActionType Type { get; set; }
        public string Message { get; set; }
        public string IP { get; set; }
        public DateTime CreatedAt { get; set; }
        public string FormatedDate { get; set; }
    }

    public enum ActionType
    {
        SelfiesCreated = 1,
        SelfiesUpdated = 2,
        SelfiesDeleted = 3,

        LoggedIn = 4,
        LoggedOut = 5,

        TwitterConnect = 6,
        TwitterSwitched = 7,
        TwitterActivate = 8,
        TwitterDeactivate = 9
    }
}