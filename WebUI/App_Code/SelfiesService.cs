﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using SocialMediaLibrary;
using Newtonsoft.Json;
using Selfies.Entities;
using Selfies.Dao;
using System.IO;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class SelfiesService : System.Web.Services.WebService {
    /// <summary>
    /// Get Evercam token details from given token_endpoint
    /// </summary>
    /// <param name="token_endpoint">https://{api_url}/oauth2/tokeninfo?access_token={access_token}</param>
    /// <returns>See sample response data below</returns>
    [WebMethod]
    public TokenUserModel GetTokenUser(string token_endpoint)
    {
        try
        {
            token_endpoint = token_endpoint.Replace("dashboard", "api");
            string result;
            WebRequest r = WebRequest.Create(token_endpoint);
            r.Method = "GET";
            using (var response = (HttpWebResponse)r.GetResponse())
            {
                result = new StreamReader(response.GetResponseStream()).ReadToEnd();
                response.Close();
            }
            TokenUserModel token = JsonConvert.DeserializeObject<TokenUserModel>(result);
            ////// UPDATES USER ACCESS TOKEN AGAINST ALL SELFIES ////////
            SelfiesTweetDao.UpdateUserToken(token.userid, token.access_token);
            /////////////////////////////////////////////////////////////
            return token;
        }
        catch (Exception x) { throw new Exception(x.Message); }
    }

    [WebMethod]
    public string[] GetURL()
    {
        TwitterInfo ti = new TwitterInfo();
        var url = ti.GetOAuthUrl();
        return new string[] { ti.getTokenSecret(), url.Replace("http", "https") };
    }

    [WebMethod]
    public void GetOauthURL(string evercamId, string twitterId)
    {
        TwitterDao.DeleteTwitterAccount(twitterId);
        SelfiesTweetDao.UpdateSelfiesTwiterId(evercamId, string.Empty);
        LogsDao.InsertLog(evercamId, twitterId, "<b>" + evercamId + "</b> deactivated twitter account <b>@" + twitterId + "</b>", UserIp(), ActionType.TwitterDeactivate);
        try
        {
            var fileName = @"C:\Selfies\SelfiesApp\twitters\" + twitterId + ".twitter";
            if (File.Exists(fileName))
                File.Delete(fileName);
            LogError("Delete " + twitterId + ".twitter file.");
        }
        catch (Exception e) { LogError(e.ToString()); }
    }

    [WebMethod]
    public string[] Authorise(string token, string verifier, string evercamId)
    {
        TwitterInfo ti = new TwitterInfo();
        var twitterInfo = ti.Authorize(token, verifier, evercamId, UserIp());
        if (!string.IsNullOrEmpty(twitterInfo[1]) && twitterInfo[0] != "exist")
        {
            try
            {
                var fileName = @"C:\Selfies\SelfiesApp\twitters\" + twitterInfo[1] + ".twitter";
                if (!File.Exists(fileName))
                {
                    StreamWriter file = new StreamWriter(fileName, true);
                    file.WriteLine(twitterInfo[1]);
                    file.Close();
                    LogError("Create " + twitterInfo[1] + ".twitter file.");
                }
            }
            catch (Exception e) { LogError("Error while creating file: " + e.Message); }
        }
        return new string[] { twitterInfo[0], twitterInfo[1] };
    }

    [WebMethod]
    public int SaveSelfieDetails(int selfiesId, string twitterId, string cameraId, string evercamId, string hashTag, string photoHeading, string logoFile, string snapshotUrl, string userName, string password, string response, string accessToken)
    {
        SelfiesTweet selfiesTweet = new SelfiesTweet();
        selfiesTweet.TwitterId = twitterId;
        selfiesTweet.CameraId = cameraId;
        selfiesTweet.SnapshotUrl = snapshotUrl;
        selfiesTweet.HashTag = hashTag;
        selfiesTweet.PhotoText = photoHeading;
        selfiesTweet.Username = userName;
        selfiesTweet.Password = password;
        selfiesTweet.IsActive = true;
        selfiesTweet.LogoFile = logoFile;
        selfiesTweet.Response = response;
        selfiesTweet.EvercamId = evercamId;
        selfiesTweet.AccessToken = accessToken;
        
        if (selfiesId > 0)
        {
            selfiesTweet.Id = selfiesId;
            if (SelfiesTweetDao.UpdateSelfiesTweetById(selfiesTweet))
            {
                saveTagFile(selfiesTweet.HashTag, twitterId, selfiesTweet.Id, evercamId);
                LogsDao.InsertLog(evercamId, twitterId, "<b>" + evercamId + "</b> changed selfies settings for camera <b>" + cameraId + "</b> with <b>" + (string.IsNullOrEmpty(hashTag) ? "@" + twitterId : hashTag) + "</b>", UserIp(), ActionType.SelfiesUpdated);
            }
            else
                LogError("SaveSelfieDetails (" + selfiesTweet.Id + ") failed");
            //LogsDao.InsertLog(evercamId, twitterId, "[" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss") + "] [" + UserIp() + "] " + evercamId + "  changed selfies settings for camera " + cameraId + " with " + (string.IsNullOrEmpty(hashTag) ? "@" + twitterId : hashTag), UserIp());
        }
        else
        {
            if (SelfiesTweetDao.GetSelfiesTweetByHash(hashTag, twitterId, evercamId).Id > 0)
            {
                if (SelfiesTweetDao.UpdateSelfiesTweet(selfiesTweet))
                {
                    saveTagFile(selfiesTweet.HashTag, twitterId, selfiesTweet.Id, evercamId);
                    LogsDao.InsertLog(evercamId, twitterId, "<b>" + evercamId + "</b> changed selfies settings for camera <b>" + cameraId + "</b> with <b>" + (string.IsNullOrEmpty(hashTag) ? "@" + twitterId : hashTag) + "</b>", UserIp(), ActionType.SelfiesUpdated);
                }
                else
                    LogError("SaveSelfieDetails UpdateSelfiesTweet (" + selfiesTweet.Id + ") failed");
            }
            else
            {
                if (SelfiesTweetDao.InsertSelfiesTweet(selfiesTweet)) {
                    saveTagFile(selfiesTweet.HashTag, twitterId, selfiesTweet.Id, evercamId);
                    LogsDao.InsertLog(evercamId, twitterId, "<b>" + evercamId + "</b>  added new selfies for camera <b>" + cameraId + "</b> with <b>" + (string.IsNullOrEmpty(hashTag) ? "@" + twitterId : hashTag) + "</b>", UserIp(), ActionType.SelfiesCreated);
                }
                else
                    LogError("SaveSelfieDetails InsertSelfiesTweet (" + selfiesTweet.Id + ") failed");
            }
        }
        
        return selfiesTweet.Id;
    }

    public static void saveTagFile(string hash, string twitterId, int selfiesId, string evercamId)
    {
        try
        {
            //if (!string.IsNullOrEmpty(hash))
            //{
                var fileName = @"C:\Selfies\SelfiesApp\tags\" + selfiesId + ".tag";
                string previousHash = "";
                if (File.Exists(fileName))
                {
                    string[] lines = File.ReadAllLines(fileName);
                    foreach (string line in lines)
                    {
                        previousHash = line;
                        break;
                    }
                    File.Delete(fileName);
                }
                
                StreamWriter file = new StreamWriter(fileName, true);
                file.WriteLine(evercamId + '@' + twitterId + hash);
                if (!string.IsNullOrEmpty(previousHash) && previousHash != evercamId + '@' + twitterId + hash)
                    file.WriteLine(previousHash);
                file.Close();
            //}
        }
        catch (Exception e) { LogError(e.ToString()); }

        try
        {
            var fileName = @"C:\Selfies\SelfiesApp\twitters\" + twitterId + ".twitter";
            if (File.Exists(fileName))
                return;
            StreamWriter file = new StreamWriter(fileName, true);
            file.WriteLine(twitterId);
            file.Close();
            LogError("Create " + twitterId + ".twitter file.");
        }
        catch (Exception e) { }
    }

    public static void deleteTagFile(int selfiesId)
    {
        try
        {
            var fileName = @"C:\Selfies\SelfiesApp\tags\" + selfiesId + ".tag";
            if (File.Exists(fileName))
                File.Delete(fileName);
        }
        catch (Exception e) { LogError(e.ToString()); }
    }

    public static void LogError(string message)
    {
        try
        {
            var fileName = @"C:\selfiesdemo\logs\selfiesservice.txt";
            StreamWriter file = new StreamWriter(fileName, true);
            file.WriteLine(message + "\t" + DateTime.UtcNow + "\n");
            file.Close();
        }
        catch (Exception) { }

    }

    private string UserIp()
    {
        HttpContext context = HttpContext.Current;
        return context.Request.UserHostAddress;
    }

    [WebMethod]
    public string GetList(string evercamId)
    {
        var selfiesTweets = SelfiesTweetDao.GetAllSelfiesTweet(evercamId);
        return JsonConvert.SerializeObject(selfiesTweets);
    }

    [WebMethod]
    public string GetTwitterInfo(string evercamId)
    {
        var twitter = TwitterDao.GetTwiterByEvercamId(evercamId);
        return JsonConvert.SerializeObject(twitter);
    }

    [WebMethod]
    public string saveResponses(string[][] responses, string evercamId)
    {
        for (int i = 0; i < responses.Length; i++)
        {
            //TwitterDao.UpdateTwitterResponse(responses[i][0], responses[i][1], evercamId);
            LogsDao.InsertLog(evercamId, responses[i][0], "<b>" + evercamId + "</b> updated tweet response message for <b>@" + responses[i][0] + "</b>", UserIp(), ActionType.SelfiesUpdated);
            //LogsDao.InsertLog(evercamId, responses[i][0], "[" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss") + "] [" + UserIp() + "] " + evercamId + " updated tweet response message for @" + responses[i][0], UserIp());
        }
        return "updated";
    }

    [WebMethod]
    public string GetLogs(string evercamId, string filter)
    {
        var logs = LogsDao.GetLogsByEvercamId(evercamId, filter);
        return JsonConvert.SerializeObject(logs);
    }

    [WebMethod]
    public void DeleteSelfiesTweet(int selfiesId, string evercamId, string cameraId, string hashTag, string twitterId)
    {
        SelfiesTweetDao.DeleteSelfiesLogically(selfiesId);
        LogsDao.InsertLog(evercamId, twitterId, "<b>" + evercamId + "</b> delete selfies for camera <b>" + cameraId + "</b> with " + (string.IsNullOrEmpty(hashTag) ? "<b>@" + twitterId + "</b>" : "<b>" + hashTag + "</b>"), UserIp(), ActionType.SelfiesDeleted);
        //LogsDao.InsertLog(evercamId, twitterId, "[" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss") + "] [" + UserIp() + "] " + evercamId + " delete selfies for camera " + cameraId + " with " + (string.IsNullOrEmpty(hashTag) ? "@" + twitterId : hashTag), UserIp());
        deleteTagFile(selfiesId);
    }

    [WebMethod]
    public void LoginLogoutLog(string logType, string evercamId)
    {
        if (logType == "login")
            LogsDao.InsertLog(evercamId, string.Empty, "<b>" + evercamId + "</b> logged in ", UserIp(), ActionType.LoggedIn); //"[" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss") + "] [" + UserIp() + "] " + 
        else
            LogsDao.InsertLog(evercamId, string.Empty, "<b>" + evercamId + "</b> logged out ", UserIp(), ActionType.LoggedOut); //"[" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss") + "] [" + UserIp() + "] " + 
    }

    [WebMethod]
    public void ActiveSelfies(int selfiesId, bool isActive, string hashTag, string evercamId)
    {
        SelfiesTweetDao.ActiveSelfies(selfiesId, isActive);
        if (isActive)
            LogsDao.InsertLog(evercamId, string.Empty, "<b>" + evercamId + "</b> Activate selfies <b>" + hashTag + "</b>", UserIp(), ActionType.SelfiesUpdated); //"[" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss") + "] [" + UserIp() + "] " + 
        else
            LogsDao.InsertLog(evercamId, string.Empty, "<b>" + evercamId + "</b> DeActivate selfies <b>" + hashTag + "</b>", UserIp(), ActionType.SelfiesUpdated); //"[" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss") + "] [" + UserIp() + "] " + 
    }
}
