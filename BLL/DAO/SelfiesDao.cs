﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using BLL.Entities;

namespace BLL.DAO
{
    public class SelfiesDao
    {
        public static string LastError = "";

        public static Selfies Get(int id)
        {
            LastError = "";
            var self = new Selfies();
            try
            {
                const string sql = "Select * FROM Selfies WHERE Id=@Id AND IsDeleted=0";
                var p1 = new SqlParameter("@Id", id);
                var cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.Add(p1);
                cmd.Connection = Connection.DbConnection;
                Connection.OpenConnection();
                var dr = GetListFromDataReader(cmd.ExecuteReader());

                if (dr.Count > 0) self = dr.FirstOrDefault();

                Connection.CloseConnection();
                return self;
            }
            catch (Exception ex)
            {
                LastError = string.Format("SelfiesDao Get(int id) id={0}<br />{1}", id, ex.Message);
                return self;
            }
            finally
            {
                Connection.CloseConnection();
            }
        }

        public static Selfies Get(string twitterId, string hashTag)
        {
            LastError = "";
            var self = new Selfies();
            try
            {
                const string sql = "Select * FROM Selfies WHERE TwitterId=@TwitterId AND HashTag=@HashTag AND IsDeleted=0";
                var p2 = new SqlParameter("@TwitterId", twitterId);
                var p3 = new SqlParameter("@HashTag", string.IsNullOrEmpty(hashTag) ? DBNull.Value.ToString(): hashTag);
                var cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.Add(p2);
                cmd.Parameters.Add(p3);
                cmd.Connection = Connection.DbConnection;
                Connection.OpenConnection();
                var dr = GetListFromDataReader(cmd.ExecuteReader());

                if (dr.Count > 0) self = dr.FirstOrDefault();

                Connection.CloseConnection();
                return self;
            }
            catch (Exception ex)
            {
                LastError = string.Format("SelfiesDao Get(string twitterId, string hashTag) twitterId={0}, hashTag={1}<br />{2}", twitterId, hashTag, ex.Message);
                return self;
            }
            finally
            {
                Connection.CloseConnection();
            }
        }

        public static Selfies Get(string twitterId)
        {
            LastError = "";
            var self = new Selfies();
            try
            {
                const string sql = "Select * FROM Selfies WHERE TwitterId=@TwitterId AND ISNULL(NULLIF(HashTag,''),'')='' AND IsDeleted=0";
                var p2 = new SqlParameter("@TwitterId", twitterId);
                var cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.Add(p2);
                cmd.Connection = Connection.DbConnection;
                Connection.OpenConnection();
                var dr = GetListFromDataReader(cmd.ExecuteReader());

                if (dr.Count > 0) self = dr.FirstOrDefault();

                Connection.CloseConnection();
                return self;
            }
            catch (Exception ex)
            {
                LastError = string.Format("SelfiesDao Get(string twitterId, string hashTag) twitterId={0} <br />{1}", twitterId, ex.Message);
                return self;
            }
            finally
            {
                Connection.CloseConnection();
            }
        }

        public static Selfies GetDeleted(int id)
        {
            LastError = "";
            var self = new Selfies();
            try
            {
                const string sql = "Select * FROM Selfies WHERE Id=@Id AND IsDeleted=1";
                var p1 = new SqlParameter("@Id", id);
                var cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.Add(p1);
                cmd.Connection = Connection.DbConnection;
                Connection.OpenConnection();
                var dr = GetListFromDataReader(cmd.ExecuteReader());

                if (dr.Count > 0) self = dr.FirstOrDefault();

                Connection.CloseConnection();
                return self;
            }
            catch (Exception ex)
            {
                LastError = string.Format("SelfiesDao Get(int id) id={0}<br />{1}", id, ex.Message);
                return self;
            }
            finally
            {
                Connection.CloseConnection();
            }
        }

        public static List<Selfies> GetList(bool activeOnly)
        {
            LastError = "";
            try
            {
                string sql = "Select * FROM Selfies WHERE IsDeleted=0 ";
                if (activeOnly)
                    sql += " AND IsActive=1";
                var cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Connection = Connection.DbConnection;
                Connection.OpenConnection();
                var dr = GetListFromDataReader(cmd.ExecuteReader());
                Connection.CloseConnection();
                return dr;
            }
            catch (Exception ex)
            {
                LastError = string.Format("SelfiesDao GetList(bool activeOnly) <br />{1}", ex.Message);
                return new List<Selfies>();
            }
            finally
            {
                Connection.CloseConnection();
            }
        }

        public static List<Selfies> GetList(string twitterid, bool activeOnly)
        {
            LastError = "";
            try
            {
                string sql = "Select * FROM Selfies WHERE TwitterId=@TwitterId " + (activeOnly ? "AND IsActive=1" : "") + " AND IsDeleted=0 ORDER BY HashTag";
                var cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                var p2 = new SqlParameter("@TwitterId", twitterid);
                cmd.Parameters.Add(p2);
                cmd.Connection = Connection.DbConnection;
                Connection.OpenConnection();
                var dr = GetListFromDataReader(cmd.ExecuteReader());
                Connection.CloseConnection();
                return dr;
            }
            catch (Exception ex)
            {
                LastError = string.Format("SelfiesDao GetList() <br />{1}", ex.Message);
                return new List<Selfies>();
            }
            finally
            {
                Connection.CloseConnection();
            }
        }

        public static Selfies GetDefault(string twitterid)
        {
            LastError = "";
            try
            {
                string sql = "Select * FROM Selfies WHERE TwitterId=@TwitterId AND IsActive=1 AND IsDeleted=0 AND (ISNULL(HashTag, '') = '') ORDER BY HashTag";
                var cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                var p2 = new SqlParameter("@TwitterId", twitterid);
                cmd.Parameters.Add(p2);
                cmd.Connection = Connection.DbConnection;
                Connection.OpenConnection();
                var dr = GetListFromDataReader(cmd.ExecuteReader());
                Connection.CloseConnection();
                return dr.FirstOrDefault<Selfies>();
            }
            catch (Exception ex)
            {
                LastError = string.Format("SelfiesDao GetList() <br />{1}", ex.Message);
                return new Selfies();
            }
            finally
            {
                Connection.CloseConnection();
            }
        }

        public static bool Delete(int id)
        {
            LastError = "";
            var self = new Selfies();
            try
            {
                const string sql = "DELETE Selfies WHERE Id=@Id";
                var p1 = new SqlParameter("@Id", id);
                var cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.Add(p1);
                cmd.Connection = Connection.DbConnection;
                Connection.OpenConnection();
                int r = cmd.ExecuteNonQuery();
                Connection.CloseConnection();
                return (r > 0);
            }
            catch (Exception ex)
            {
                LastError = string.Format("SelfiesDao Delete(int id) id={0}<br />{1}", id, ex.Message);
                return false;
            }
            finally
            {
                Connection.CloseConnection();
            }
        }

        public static bool Delete(string twitterId)
        {
            LastError = "";
            var self = new Selfies();
            try
            {
                const string sql = "DELETE Selfies WHERE TwitterId=@TwitterId";
                var p1 = new SqlParameter("@TwitterId", twitterId);
                var cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.Add(p1);
                cmd.Connection = Connection.DbConnection;
                Connection.OpenConnection();
                int r = cmd.ExecuteNonQuery();
                Connection.CloseConnection();
                return (r > 0);
            }
            catch (Exception ex)
            {
                LastError = string.Format("SelfiesDao Delete(string evercamid) TwitterId={0}<br />{1}", twitterId, ex.Message);
                return false;
            }
            finally
            {
                Connection.CloseConnection();
            }
        }

        private static List<Selfies> GetListFromDataReader(SqlDataReader dr)
        {
            LastError = "";
            List<Selfies> selfies = new List<Selfies>();
            try
            {
                while (dr.Read())
                {
                    var self = new Selfies();
                    if (!dr.IsDBNull(dr.GetOrdinal("Id")))
                        self.ID = dr.GetInt32(dr.GetOrdinal("Id"));

                    if (!dr.IsDBNull(dr.GetOrdinal("CameraId")))
                        self.CameraID = dr["CameraId"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("TwitterId")))
                        self.TwitterID = dr["TwitterId"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("PhotoText")))
                        self.PhotoText = dr["PhotoText"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("HashTag")))
                        self.HashTag = dr["HashTag"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("LogoFile")))
                        self.LogoFile = dr["LogoFile"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("SnapshotUrl")))
                        self.SnapshotUrl = dr["SnapshotUrl"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("Username")))
                        self.Username = dr["Username"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("Password")))
                        self.Password = dr["Password"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("EvercamId")))
                        self.EvercamID = dr["EvercamId"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("AccessToken")))
                        self.AccessToken = dr["AccessToken"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("Response")))
                        self.Response = dr["Response"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("CreatedAt")))
                        self.CreatedAt = dr.GetDateTime(dr.GetOrdinal("CreatedAt"));

                    if (!dr.IsDBNull(dr.GetOrdinal("IsActive")))
                        self.IsActive = dr.GetBoolean(dr.GetOrdinal("IsActive"));
                    if (!dr.IsDBNull(dr.GetOrdinal("IsDeleted")))
                        self.IsDeleted = dr.GetBoolean(dr.GetOrdinal("IsDeleted"));

                    selfies.Add(self);
                }
                dr.Close();
                dr.Dispose();
            }
            catch (Exception ex)
            {
                LastError = string.Format("SelfiesDao GetListFromDataReader(SqlDataReader dr)<br />" + ex.ToString());
            }
            return selfies;
        }
    }
}
