using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Selfies
{
    public class DbConnection
    {
        [ThreadStatic]
        private static String _connectionString;
        [ThreadStatic]
        private static SqlTransaction _transaction;
        [ThreadStatic]
        private static SqlConnection _connection;
        [ThreadStatic]
        private static int _transLocks;
        [ThreadStatic]
        private static int _cnLocks;
        [ThreadStatic]
        private static bool _rollbackVote;
        
        
        public static SqlConnection Connection
        {
            get
            {
                
                if (_connection == null)
                {
                    _connectionString = ConfigurationManager.AppSettings["ConnectionString"];
                    _connection = new SqlConnection(_connectionString);
                    return _connection;
                }
                return _connection;
                               
            }
        }

        public static void OpenConnection()
        {
            if (_cnLocks == 0)
                Connection.Open();
            if (Connection.State != ConnectionState.Open) //todo remove this ?
                throw new ApplicationException("Assert: Connection is not open");
            _cnLocks++;
            if (_cnLocks >= (int.MaxValue - 100))
                _cnLocks = 1;
        }
        
        public static void CloseConnection()
        {
            if (_cnLocks > 0)
            {
                Connection.Close();
                Connection.Dispose();
                _transaction = null;
                _connection = null;
                _cnLocks--;
            }
        }
        
        //TODO Isolation level
        public static void BeginTransaction()
        {
            if (_transLocks == 0)
                _transaction = Connection.BeginTransaction();
            _transLocks++;
            _rollbackVote = false;
        }

        public static SqlTransaction Transaction
        {
            get
            {
                return _transaction;
            }
        }

        public static void CommitTransaction()
        {
            _transLocks--;
            if (_transLocks == 0)
            {
                if (_rollbackVote)
                    _transaction.Rollback();
                else
                    _transaction.Commit();
            }
        }

        public static void RollbackTransaction()
        {
            _rollbackVote = true;
            _transLocks--;
            if (_transLocks == 0)
                _transaction.Rollback();
        }

        public static void ShrinkLogFile()
        {
            try
            {
                const string query = @"DBCC SHRINKFILE (@LogFileName)";
                SqlCommand cmd = new SqlCommand { CommandText = query, CommandTimeout = 0, CommandType = CommandType.Text };
                OpenConnection();
                cmd.Connection = Connection;
                var db = Connection.Database + "_log";
                cmd.Parameters.AddWithValue("@LogFileName", db);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (SqlException ex)
            {
                string msg = ("DbConnection ShrinkLogFile" + ex.Message);
            }
            finally
            {
                CloseConnection();
            }
        }
    }
}
