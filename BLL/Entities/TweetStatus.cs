﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Entities
{
    public class TweetStatus
    {

        public long Tid;

        public string Message;

        public string[] Mentions;

        public string[] Tags;

        public string Creator;

        public string Listener;
    }
}
