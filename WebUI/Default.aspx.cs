﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Selfies.Entities;
using Selfies.Dao;
using SocialMediaLibrary;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        TwitterInfo ti = new TwitterInfo();
        var url = ti.GetOAuthUrl();
        var s = new string[] { ti.getTokenSecret(), url.Replace("http", "https") };
    }
}