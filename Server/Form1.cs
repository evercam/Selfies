using System;
using System.Drawing;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Net;
using BLL;
using BLL.DAO;
using BLL.Entities;
using AppModule.InterProcessComm;
using AppModule.NamedPipes;

namespace Server
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
        private static int total = 0;
        private bool synchronizer = false;
        private static string LogPath = "";
        private static string ListenerProcess = Utils.ListenerProcess;
        private static string ListenerPath = Utils.ListenerPath;

        public static Dictionary<string, ListenerProcessInfo> _processInfos;
        public static Dictionary<string, Selfies> _Selfies;
        public static Dictionary<string, Twitter> _Twitters;
        
        private static FileSystemWatcher selfiesWatcher = new FileSystemWatcher();
        private static FileSystemWatcher twitterWatcher = new FileSystemWatcher();

		private System.Windows.Forms.TextBox Activity;
		public static System.Windows.Forms.TextBox ActivityRef;

		public static IChannelManager PipeManager;
        private Label label1;
        private Label lblTwittersPath;
        private Label lblSelfiesPath;
        private Label label3;
        private Button btnListener;
        private Timer _timer;
        private Button btnFile;
        private Label label2;
        private FolderBrowserDialog folderBrowser;
        private LinkLabel lnkLabel;
        private IContainer components;

		public Form1()
		{
            InitializeComponent();

            ActivityRef = this.Activity;
            PipeManager = new PipeManager();
            PipeManager.Initialize();

            selfiesWatcher.Path = Utils.HashFileWatchPath;
            selfiesWatcher.Filter = "*.tag";
            selfiesWatcher.IncludeSubdirectories = true;
            selfiesWatcher.Created += new FileSystemEventHandler(OnSelfiesCreated);
            selfiesWatcher.Deleted += new FileSystemEventHandler(OnSelfiesDeleted);
            selfiesWatcher.EnableRaisingEvents = true;
            lblSelfiesPath.Text = Utils.HashFileWatchPath;

            twitterWatcher.Path = Utils.TwitterFileWatchPath;
            twitterWatcher.Filter = "*.twitter";
            twitterWatcher.IncludeSubdirectories = true;
            twitterWatcher.Created += new FileSystemEventHandler(OnTwitterCreated);
            twitterWatcher.Deleted += new FileSystemEventHandler(OnTwitterDeleted);
            twitterWatcher.EnableRaisingEvents = true;
            lblTwittersPath.Text = Utils.TwitterFileWatchPath;

            lnkLabel.Text = LogPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\logs";
            if (!Directory.Exists(LogPath))
                Directory.CreateDirectory(LogPath);

            Init();
		}

        void OnSelfiesCreated(object sender, FileSystemEventArgs e)
        {
            string tag = "";    // newmention#newtag        //evercamid@newmention#newtag  or  evercamid@newmention
            string tag2 = "";   // oldmention#oldtag        //evercamid@oldmention#oldtag  or  evercamid@oldmention
            try
            {
                System.Threading.Thread.Sleep(500);
                string fileName = Path.Combine(Utils.HashFileWatchPath, e.Name);
                int id = int.Parse(Path.GetFileNameWithoutExtension(fileName));

                using (FileStream fs = new FileStream(fileName, FileMode.Open))
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        tag = sr.ReadLine();
                        tag2 = sr.ReadLine();
                    }
                }

                Utils.DBLog(LogTypes.Debug, "OnSelfiesCreated... tag1#" + tag + ", tag2#" + tag2, 0, "", "", "Server");

                if (string.IsNullOrEmpty(tag))
                    return;

                tag = tag.Trim().ToLower();
                string[] split = tag.Split(new char[] { '@', '#' });
                string eid = split[0];
                string tid = split[1];
                string hash = "";
                if (split.Count() > 2)
                    hash = "#" + split[2];
                string key = Utils.SelfiesKey(tid, hash);

                if (!string.IsNullOrEmpty(tag2))
                {
                    var _s = _Selfies.Where(s => s.Value.ID.Equals(id)).FirstOrDefault();
                    if (_s.Key != null && _s.Value != null)
                    {
                        _Selfies.Remove(_s.Key);
                        ActivityRef.AppendText("Tag cleared (" + id + "): " + _s.Key + Environment.NewLine);
                    }

                    Selfies self = SelfiesDao.Get(id);
                    if (!string.IsNullOrEmpty(SelfiesDao.LastError))
                        ActivityRef.AppendText("SelfiesDao.Get(" + id + ") Error: " + SelfiesDao.LastError + Environment.NewLine);
                    if (self.ID > 0)
                    {
                        _Selfies.Add(key, self);
                        ActivityRef.AppendText("Tag upserted (" + self.ID + "): " + key + Environment.NewLine);
                    }
                }
                else if (_Selfies.Keys.Contains(key))
                {
                    tag2 = "";
                    var _s = _Selfies.Where(s => s.Value.ID.Equals(id)).FirstOrDefault();
                    if (_s.Key != null && _s.Value != null)
                    {
                        Selfies self = new Selfies();
                        if (key.Contains("#"))
                            self = SelfiesDao.Get(_Selfies[_s.Key].TwitterID, _Selfies[_s.Key].HashTag);
                        else
                            self = SelfiesDao.Get(_Selfies[_s.Key].TwitterID);
                        if (!string.IsNullOrEmpty(SelfiesDao.LastError))
                            ActivityRef.AppendText("SelfiesDao.GetList(" + _Selfies[_s.Key].HashTag + ") Error: " + SelfiesDao.LastError + Environment.NewLine);
                        if (self.ID > 0)
                        {
                            _Selfies[key] = self;
                            ActivityRef.AppendText("Tag modified (" + self.ID + "): " + key + Environment.NewLine);
                        }
                        else
                            ActivityRef.AppendText("Tag not modified (" + id + "): " + key + Environment.NewLine);
                    }
                    else
                        ActivityRef.AppendText("Tag missed (" + id + "): " + key + Environment.NewLine);
                }
                else if (!_Selfies.Keys.Contains(key))
                {
                    tag2 = "";
                    Selfies self = new Selfies();
                    if (key.Contains("#"))
                        self = SelfiesDao.Get(tid, hash);
                    else
                        self = SelfiesDao.Get(tid);
                    if (!string.IsNullOrEmpty(SelfiesDao.LastError))
                    {
                        ActivityRef.AppendText("SelfiesDao.Get(" + hash + ") Error: " + SelfiesDao.LastError + Environment.NewLine);
                    }
                    if (self.ID > 0)
                    {
                        _Selfies.Add(key, self);
                        ActivityRef.AppendText("Tag added (" + self.ID + "): " + key + Environment.NewLine);
                    }
                    else
                    {
                        ActivityRef.AppendText("Tag dropped (" + self.ID + "): " + key + Environment.NewLine);
                    }
                }
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "OnSelfiesCreated... [Error] " + x.ToString(), "Server");
                ActivityRef.AppendText("Error OnSelfiesCreated: " + x.Message + " (tag1: " + tag + ")" + " (tag2: " + tag2 + ")" + Environment.NewLine);
            }
        }

        void OnSelfiesDeleted(object sender, FileSystemEventArgs e)
        {
            int id = 0;
            try
            {
                string fileName = Path.Combine(Utils.HashFileWatchPath, e.Name);
                id = int.Parse(Path.GetFileNameWithoutExtension(fileName));
                //// if user has marked the tag for deletion instead of 
                //// deleted file only while updating it
                Utils.DBLog(LogTypes.Debug, "OnSelfiesDeleted... id:" + id, 0, "", "", "Server");
                Selfies self = SelfiesDao.GetDeleted(id);
                if (!string.IsNullOrEmpty(SelfiesDao.LastError))
                    ActivityRef.AppendText("SelfiesDao.GetDeleted(" + id + ") Error: " + SelfiesDao.LastError + Environment.NewLine);
                if (self.ID > 0)
                {
                    SelfiesDao.Delete(self.ID);
                    _Selfies.Remove(Utils.SelfiesKey(self));
                    ActivityRef.AppendText("Tag deleted (" + self.ID + "): " + self.EvercamID + "@" + self.TwitterID + self.HashTag + Environment.NewLine);
                }
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "OnSelfiesDeleted... Error: " + x.ToString(), 0, "", "", "Server");
                ActivityRef.AppendText("Error OnSelfiesDeleted: " + x.Message + " (id: " + id + ")" + Environment.NewLine);
            }
        }

        void OnTwitterCreated(object sender, FileSystemEventArgs e)
        {
            string twitterid = "";
            try
            {
                System.Threading.Thread.Sleep(500);
                string fileName = Path.Combine(Utils.TwitterFileWatchPath, e.Name);
                using (FileStream fs = new FileStream(fileName, FileMode.Open))
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        fileName = Path.GetFileNameWithoutExtension(fileName);
                        string[] split = fileName.Split(new char[] {'.'});
                        twitterid = split[0];

                        Utils.DBLog(LogTypes.Debug, "OnTwitterCreated... twitterid:" + twitterid, 0, "", "", "Server");
                        if (string.IsNullOrEmpty(twitterid))
                        {
                            ActivityRef.AppendText("Invalid twitter file contents '" + fileName + "'" + Environment.NewLine);
                            return;
                        }

                        Twitter twitter = TwitterDao.Get(twitterid.ToLower());
                        if (!string.IsNullOrEmpty(SelfiesDao.LastError))
                            ActivityRef.AppendText("TwitterDao.Get(" + twitterid.ToLower() + ") Error: " + SelfiesDao.LastError + Environment.NewLine);
                        if (!string.IsNullOrEmpty(twitter.ID))
                        {
                            if (StartListener(twitter).ProcessId > 0)
                            {
                                if (!_Twitters.ContainsKey(Utils.ProcessKey(twitter)))
                                    _Twitters.Add(Utils.ProcessKey(twitter), twitter);

                                ActivityRef.AppendText("Twitter added: @" + twitterid.ToLower() + Environment.NewLine);
                            }
                            else
                                ActivityRef.AppendText("Twitter not added: @" + twitterid.ToLower() + Environment.NewLine);
                        }
                        else
                            ActivityRef.AppendText("Twitter dropped: @" + twitterid.ToLower() + Environment.NewLine);
                    }
                }
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "OnTwitterCreated... Error: " + x.ToString(), 0, "", "", "Server");
                ActivityRef.AppendText("Error OnTwitterCreated: " + x.Message + " (@" + twitterid + ")" + Environment.NewLine);
            }
        }

        void OnTwitterDeleted(object sender, FileSystemEventArgs e)
        {
            string twitterid = "";
            string key = "";
            try
            {
                string fileName = Path.Combine(Utils.HashFileWatchPath, e.Name);
                fileName = Path.GetFileNameWithoutExtension(fileName);

                string[] split = fileName.Split(new char[] { '.' });  //twitterid.twitter
                twitterid = split[0];

                Utils.DBLog(LogTypes.Debug, "OnTwitterDeleted... twitterid:" + twitterid, 0, "", "", "Server");
                key = Utils.ProcessKey(twitterid);
                if (_processInfos.ContainsKey(key))
                {
                    ListenerProcessInfo tpi = _processInfos[key];
                    KillProcess(tpi.ProcessId, key);

                    //// delete all selfies of a user permanently
                    //SelfiesDao.Delete(twitterid);
                    ActivityRef.AppendText("Twitter @" + twitterid + " deleted" + Environment.NewLine);
                }

                if (_Twitters.ContainsKey(key))
                    _Twitters.Remove(key);
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "OnTwitterDeleted... Error: " + x.ToString(), 0, "", "", "Server");
                ActivityRef.AppendText("Error OnTwitterDeleted: " + x.Message + " (key: " + key + ")" + Environment.NewLine);
            }
        }

        internal static ListenerProcessInfo StartListener(Twitter twitter)
        {
            string key = Utils.ProcessKey(twitter);
            ListenerProcessInfo tpi = new ListenerProcessInfo();
            try
            {
                if (!_processInfos.ContainsKey(key))
                {
                    if (twitter == null || string.IsNullOrEmpty(twitter.ID))
                    {
                        Utils.DBLog(LogTypes.Debug, "Twitter account details not found", "Server");
                        ActivityRef.AppendText("Twitter account details not found");
                        return tpi;
                    }

                    if (!twitter.IsActive)
                    {
                        Utils.DBLog(LogTypes.Debug, "Twitter account disabled", "Server");
                        ActivityRef.AppendText("Twitter account disabled");
                        return tpi;
                    }

                    if (string.IsNullOrEmpty(twitter.AccessToken) || string.IsNullOrEmpty(twitter.TokenSecret))
                    {
                        Utils.DBLog(LogTypes.Debug, "Twitter account has invalid credentials", "Server");
                        ActivityRef.AppendText("Twitter account has invalid credentials");
                        return tpi;
                    }

                    int pid = ProcessRunning(twitter);

                    if (pid == 0)
                    {
                        if (string.IsNullOrEmpty(CopyListener(twitter)))
                        {
                            Utils.DBLog(LogTypes.Debug, "Unable to create file 'listener_" + twitter.ID + ".exe'", "Server");
                            ActivityRef.AppendText("Unable to create file 'listener_" + twitter.ID + ".exe'");
                            return tpi;
                        }

                        //// copylistener(t) should have added new process info for this twitter
                        if (_processInfos.ContainsKey(key))
                        {
                            tpi = _processInfos[key];

                            ProcessStartInfo info = new ProcessStartInfo(tpi.ProcessPath, twitter.ID.ToString() + " " + total);
                            info.UseShellExecute = true;
                            info.WindowStyle = ProcessWindowStyle.Hidden;    //ProcessWindowStyle.Normal;
                            Process process = Process.Start(info);

                            System.Threading.Thread.Sleep(1000);
                            if (!process.HasExited)
                            {
                                tpi.ProcessId = process.Id;
                                tpi.IsResponding = process.Responding;

                                _processInfos[key] = tpi;
                            }
                            else
                                ActivityRef.AppendText("Selfies.StartListener (@" + twitter.ID + ") Exited");
                        }
                        else
                        {
                            ActivityRef.AppendText("Unable to start 'listener_" + twitter.ID + ".exe'");
                            return tpi;
                        }
                    }
                    else
                    {
                        ActivityRef.AppendText("No process running " + twitter.ID + ".exe'");
                        return tpi;
                    }
                    //// !!! TEST !!!
                    //if (!_Twitters.ContainsKey(key))
                    //    _Twitters.Add(key, twitter);
                }
                else
                {
                    ActivityRef.AppendText("Process already running " + twitter.ID + ".exe'");
                    return tpi;
                }
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "StartListener @" + twitter.ID + " Error: " + x.ToString(), "Server");
                ActivityRef.AppendText("Selfies.StartListener (@" + twitter.ID + ") Error: " + x.Message + Environment.NewLine);
            }
            return tpi;
        }

        private void Stop()
        {
            ActivityRef.AppendText(Environment.NewLine + "Selfies.Stop(" + DateTime.Now.ToString() + ")" + Environment.NewLine);
            string[] keys = new string[_processInfos.Keys.Count];
            _processInfos.Keys.CopyTo(keys, 0); // k = evercamid@twitterid
            foreach (string k in keys)
            {
                ListenerProcessInfo info = _processInfos[k];
                if (info.ProcessId > 0)
                {
                    KillProcess(info.ProcessId, k);
                    ActivityRef.AppendText("Selfies.StopListeners @" + k + Environment.NewLine);
                }
            }
        }

        private static int ProcessRunning(Twitter twitter)
        {
            string key = Utils.ProcessKey(twitter.ID);
            try
            {
                int id = 0;
                if (_processInfos.ContainsKey(key))
                    id = _processInfos[key].ProcessId;

                if (id > 0)
                {
                    Process process = Process.GetProcessById(id);
                    if (process.Responding)
                    {
                        _processInfos[key].IsResponding = true;
                        _processInfos[key].ProcessId = process.Id;
                    }
                    else
                    {
                        Utils.DBLog(LogTypes.Debug, "Process ID# " + id + " Kill Not Responding: ", 0, "", twitter.ID, "Server");
                        try
                        {
                            _processInfos[key].IsResponding = false;
                            _processInfos[key].ProcessId = 0;
                            _processInfos.Remove(key);
                            KillProcess(id);
                            id = 0;
                        }
                        catch (Exception x)
                        {
                            id = 0;
                            Utils.DBLog(LogTypes.Error, "ProcessRunning... Error: " + x.ToString(), 0, "", twitter.ID, "Server");
                            ActivityRef.AppendText("Selfies.ProcessRunning Error (" + process.Id + "): " + ListenerProcess + " (Error): " + x.Message + Environment.NewLine);
                        }
                    }
                }
                else
                {
                    Process[] processlist = Process.GetProcesses();
                    foreach (Process process in processlist)
                    {
                        if (process.ProcessName.ToLower().StartsWith("listener_"))
                        {
                            string[] split = process.ProcessName.Split(new char[] { '_', '.' });
                            string tid = split[1];

                            if (tid == twitter.ID)
                            {
                                if (process.Responding)
                                {
                                    id = process.Id;
                                    break;
                                }
                                else
                                {
                                    Utils.DBLog(LogTypes.Debug, "Process Name# " + process.ProcessName + " Kill Not Responding: ", 0, "", twitter.ID, "Server");
                                    KillProcess(process.Id);
                                    id = 0;
                                    break;
                                }
                            }
                        }
                    }
                }
                return id;
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "ProcessRunning... Error: " + x.ToString(), 0, "", twitter.ID, "Server");
                ActivityRef.AppendText("Selfies.ProcessRunning (listener_" + twitter.ID + ".exe) Error: " + x.Message + Environment.NewLine);
                return 0;
            }
        }

        private static ListenerProcessInfo ProcessRunning(string processName)
        {
            ListenerProcessInfo info = new ListenerProcessInfo();
            try
            {
                Process[] processlist = Process.GetProcesses();
                foreach (Process process in processlist)
                {
                    if (process.ProcessName.ToLower().Equals(processName.ToLower()))
                    {
                        if (process.Responding)
                        {
                            info.ProcessId = process.Id;
                            info.IsResponding = process.Responding;
                            info.ProcessPath = process.StartInfo.FileName;
                        }
                        else
                        {
                            KillProcess(process.Id);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "ProcessRunning (" + processName + ") Error: " + x.ToString(), 0, "", "", "Server");
                ActivityRef.AppendText("Executor.ProcessRunning(string processName) Error (" + processName + ") : " + x.Message + Environment.NewLine);
            }
            return info;
        }

        private static ListenerProcessInfo ProcessRunning(int id)
        {
            ListenerProcessInfo info = new ListenerProcessInfo();
            try
            {
                Process[] processlist = Process.GetProcesses();
                foreach (Process process in processlist)
                {
                    if (process.Id == id)
                    {
                        if (process.Responding)
                        {
                            info.ProcessId = process.Id;
                            info.IsResponding = process.Responding;
                            info.ProcessPath = process.StartInfo.FileName;
                        }
                        else
                        {
                            KillProcess(process.Id);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "ProcessRunning (" + id + ") Error: " + x.ToString(), 0, "", "", "Server");
                ActivityRef.AppendText("Executor.ProcessRunning(int id) Error (" + id + ") : " + x.Message + Environment.NewLine);
            }
            return info;
        }

        internal static bool KillProcess(int pid, string tid)
        {
            if (pid == 0) return false;
            try
            {
                //// remove this timelapse from dictionary untill its been added in next run
                //// so that the dictioanary always contains list of timelapses with currently running processes
                if (!string.IsNullOrEmpty(tid)) _processInfos.Remove(tid);

                ProcessStartInfo start = new ProcessStartInfo();
                start.FileName = "taskkill.exe";
                start.Arguments = "/pid " + pid + " /F";
                start.UseShellExecute = false;

                Process process = new Process();
                start.CreateNoWindow = true;
                process.StartInfo = start;
                process.Start();
                process.WaitForExit(1000);

                return true;
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "KillProcess (" + pid + ", " + tid + ") Error: " + x.ToString(), 0, "", "", "Server");
                ActivityRef.AppendText("KillProcess Error: " + x.Message + Environment.NewLine);
                return false;
            }
        }

        internal static bool KillProcess(int id)
        {
            try
            {
                ProcessStartInfo start = new ProcessStartInfo();
                start.FileName = "taskkill.exe";
                start.Arguments = "/pid " + id + " /F";
                start.UseShellExecute = false;

                Process process = new Process();
                start.CreateNoWindow = true;
                process.StartInfo = start;
                process.Start();
                process.WaitForExit(1000);

                return true;
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "KillProcess (" + id + ") Error: " + x.ToString(), 0, "", "", "Server");
                ActivityRef.AppendText("KillProcess Error: " + x.Message + Environment.NewLine);
                return false;
            }
        }

        internal static bool KillProcess(string tid)
        {
            try
            {
                if (!_processInfos.ContainsKey(tid))
                    return false;

                ListenerProcessInfo info = _processInfos[tid];
                //// remove this timelapse from dictionary untill its been added in next run
                //// so that the dictioanary always contains list of timelapses with currently running processes
                _processInfos.Remove(tid);

                ProcessStartInfo start = new ProcessStartInfo();
                start.FileName = "taskkill.exe";
                start.Arguments = "/pid " + info.ProcessId + " /F";
                start.UseShellExecute = false;

                Process process = new Process();
                start.CreateNoWindow = true;
                process.StartInfo = start;
                process.Start();
                process.WaitForExit(1000);

                return true;
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "KillProcess (" + tid + ") Error: " + x.ToString(), 0, "", "", "Server");
                ActivityRef.AppendText("KillProcess Error: " + x.Message + Environment.NewLine);
                return false;
            }
        }

        private static string CopyListener(Twitter twitter)
        {
            string listener = Utils.ListenerName(twitter);
            string ExeFile = Path.Combine(ListenerPath, "Listener.exe");
            string ConfigFile = Path.Combine(ListenerPath, "Listener.exe.config");
            string PathDest = Path.Combine(ListenerPath, listener + ".exe");
            string ConfigDest = Path.Combine(ListenerPath, listener + ".exe.config");
            string key = Utils.ProcessKey(twitter.ID);
            try
            {
                // if already exists (and process is running) 
                // then kill the process and delete its exe
                if (File.Exists(PathDest) && !_processInfos.ContainsKey(key))
                {
                    try
                    {
                        KillProcess(ProcessRunning(listener).ProcessId);

                        File.Delete(PathDest);
                        File.Delete(ConfigDest);

                        File.Copy(ExeFile, PathDest, true);
                        File.Copy(ConfigFile, ConfigDest, true);
                    }
                    catch (Exception x)
                    {
                        ActivityRef.AppendText("CopyListener(" + listener + ") Error in KillProcess/File.Delete/File.Copy: " + x.ToString() + Environment.NewLine);
                        return "";
                    }
                }
                else if (!File.Exists(PathDest) && !_processInfos.ContainsKey(Utils.ProcessKey(twitter.ID)))
                {
                    File.Copy(ExeFile, PathDest, true);
                    File.Copy(ConfigFile, ConfigDest, true);
                }
            }
            catch (Exception x)
            {
                ActivityRef.AppendText("CopyListener(" + twitter.ID + ") Error: " + x.ToString() + Environment.NewLine);
                return "";
            }

            if (!string.IsNullOrEmpty(PathDest))
            {
                ListenerProcessInfo tpi = new ListenerProcessInfo();

                tpi.ProcessPath = PathDest;

                _processInfos.Add(key, tpi);
            }
            return PathDest;
        }

        private void btnListener_Click(object sender, EventArgs e)
        {
            if (btnListener.Text == "STOP")
            {
                if (_processInfos != null && _processInfos.Keys != null && _processInfos.Keys.Count > 0)
                {
                    var yes = MessageBox.Show("Are you sure to STOP all listeners ?", "Stop Listeners", MessageBoxButtons.YesNo);
                    if (yes == System.Windows.Forms.DialogResult.Yes)
                    {
                        _timer.Enabled = false;

                        Stop();

                        btnListener.Text = "START";
                    }
                }
            }
            else if (btnListener.Text == "START")
            {
                Init();

                btnListener.Text = "STOP";
            }
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            File.WriteAllText(Path.Combine(LogPath, DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt"), ActivityRef.Text);
        }

        private void lnkLabel_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(LogPath))
                Process.Start("explorer.exe", LogPath);
        }

        private void _timer_Tick(object sender, EventArgs e)
        {
            total = 0;
            //// already synchronizing
            if (!synchronizer)
            {
                synchronizer = true;

                Synch();

                synchronizer = false;
            }
        }

        private void Init()
        {
            try
            {
                Utils.DBLog(LogTypes.Debug, "Server Init...", 0, "", "", "Server");
                ActivityRef.AppendText(Environment.NewLine + "Selfies.Init(" + DateTime.Now.ToString() + ")" + Environment.NewLine);
                _processInfos = new Dictionary<string, ListenerProcessInfo>();
                _Selfies = new Dictionary<string, Selfies>();
                _Twitters = new Dictionary<string, Twitter>();

                _timer.Interval = int.Parse(Utils.SynchInterval);
                _timer.Enabled = true;
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "Server Init... Error: " + x.ToString(), 0, "", "", "Server");
                ActivityRef.AppendText("Error Init(): " + x.Message + Environment.NewLine);
            }
        }

        private void Synch()
        {
            string track = "a";
            try
            {
                //ActivityRef.AppendText("@" + DateTime.UtcNow.ToString() + Environment.NewLine);
                // loads Active selfies only
                List<Selfies> selfies = SelfiesDao.GetList(true);
                if (!string.IsNullOrEmpty(SelfiesDao.LastError))
                    Utils.DBLog(LogTypes.Error, "Synch... Last Error: " + SelfiesDao.LastError, 0, "", "", "Server");
                else
                    _Selfies = new Dictionary<string, Selfies>();   //// !!! TEST !!!
                track = "b" + selfies.Count;
                foreach (Selfies self in selfies)
                {
                    try
                    {
                        string key = Utils.SelfiesKey(self);
                        if (!_Selfies.ContainsKey(key))
                            _Selfies.Add(key, self);
                        else
                            _Selfies[key] = self;   //// !!! TEST !!!
                    }
                    catch (Exception x)
                    {
                        Utils.DBLog(LogTypes.Error, "Selfies listing... Error: " + x.ToString(), 0, "", "", "Server");
                        ActivityRef.AppendText("Error Synch(" + self.ID + "): " + x.Message + Environment.NewLine);
                    }
                }
                
                List<Twitter> twitters = TwitterDao.GetList();
                if (!string.IsNullOrEmpty(SelfiesDao.LastError))
                    Utils.DBLog(LogTypes.Error, "Synch... Error: " + SelfiesDao.LastError, 0, "", "", "Server");
                else
                    _Twitters = new Dictionary<string, Twitter>();   //// !!! TEST !!!
                track = "c" + twitters.Count;
                foreach (Twitter twit in twitters)
                {
                    string key = Utils.ProcessKey(twit);
                    
                    //// FOR TESTING ONLY
                    //if (!key.Contains("carrollsgifts"))
                    //    continue;
                    
                    track = "d" + key;
                    if (!twit.IsActive)
                    {
                        int pid = ProcessRunning(twit);
                        if (pid > 0)
                        {
                            KillProcess(pid);
                            _processInfos.Remove(key);
                            _Twitters.Remove(key);    //// !!! TEST !!!
                            ActivityRef.AppendText("STOPPROCESS (INACTIVE) @" + twit.ID + Environment.NewLine);
                        }
                        continue;
                    }

                    //// !!! TEST !!!
                    if (!_Twitters.ContainsKey(key))
                        _Twitters.Add(key, twit);
                    else
                        _Twitters[key] = twit;  //// !!! TEST !!!

                    ListenerProcessInfo cached = new ListenerProcessInfo();
                    ListenerProcessInfo info = new ListenerProcessInfo();

                    if (_processInfos.ContainsKey(key))
                    {
                        track = "e" + key;
                        cached = _processInfos[key];
                        info = ProcessRunning(cached.ProcessId);
                        if (info.ProcessId > 0 && info.IsResponding)
                            continue;
                        else if (info.ProcessId > 0 && !info.IsResponding)
                        {
                            Utils.DBLog(LogTypes.Debug, "Synch... Kill Process @" + twit.ID, 0, "", twit.ID, "Server");
                            ActivityRef.AppendText("KILLPROCESS @" + twit.ID + Environment.NewLine);
                            KillProcess(twit.ID);
                        }
                        else if (info.ProcessId == 0)
                        {
                            _processInfos.Remove(key);
                            Utils.DBLog(LogTypes.Debug, "Synch... Clean Process @" + twit.ID, 0, "", twit.ID, "Server");
                            ActivityRef.AppendText("CLEARPROCESS @" + twit.ID + Environment.NewLine);
                        }
                    }
                    else
                    {
                        track = "f" + key;
                        info = ProcessRunning("listener_" + twit.ID);
                        if (info.ProcessId > 0 && info.IsResponding)
                            continue;
                        else if (info.ProcessId > 0 && !info.IsResponding)
                        {
                            Utils.DBLog(LogTypes.Debug, "Synch... Kill Process @" + twit.ID, 0, "", twit.ID, "Server");
                            ActivityRef.AppendText("KILLPROCESS @" + twit.ID + Environment.NewLine);
                            KillProcess(twit.ID);
                        }
                    }

                    if (StartListener(twit).ProcessId > 0)
                        ActivityRef.AppendText(Utils.ServerName + ": @" + key + " is now listening..." + Environment.NewLine);
                    else
                        ActivityRef.AppendText(Utils.ServerName + ": @" + key + " 'listener_" + twit.ID + ".exe could not be started'" + Environment.NewLine);
                }
                track = "g" + _Twitters.Count;
                ActivityRef.AppendText(DateTime.Now + ": " + _Twitters.Count + " Twitters loaded..." + Environment.NewLine);
            }
            catch(Exception x)
            {
                Utils.DBLog(LogTypes.Error, "Synch... Error#" + track + ": " + x.ToString(), 0, "", "", "Server");
                ActivityRef.AppendText("Error Synch(" + track + "): " + x.Message + Environment.NewLine);
            }
        }

        [STAThread]
        static void Main()
        {
            Application.Run(new Form1());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                    components.Dispose();
                if (_timer != null)
                    _timer.Dispose();
            }
            base.Dispose(disposing);
        }

        private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_processInfos != null && _processInfos.Keys != null && _processInfos.Keys.Count > 0)
            {
                var yes = MessageBox.Show("Are you sure to STOP all listeners ?", "Stop Listeners", MessageBoxButtons.YesNo);
                if (yes == System.Windows.Forms.DialogResult.Yes)
                {
                    _timer.Enabled = false;

                    Stop();

                    btnListener.Text = "START";
                }
                else
                    e.Cancel = true;
            }
            // ???
            if (PipeManager != null)
            {
                PipeManager.Stop();
            }
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Activity = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTwittersPath = new System.Windows.Forms.Label();
            this.lblSelfiesPath = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnListener = new System.Windows.Forms.Button();
            this._timer = new System.Windows.Forms.Timer(this.components);
            this.btnFile = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.lnkLabel = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // Activity
            // 
            this.Activity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Activity.BackColor = System.Drawing.SystemColors.Info;
            this.Activity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Activity.Location = new System.Drawing.Point(12, 98);
            this.Activity.Multiline = true;
            this.Activity.Name = "Activity";
            this.Activity.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Activity.Size = new System.Drawing.Size(406, 386);
            this.Activity.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Twitters Path:";
            // 
            // lblTwittersPath
            // 
            this.lblTwittersPath.AutoSize = true;
            this.lblTwittersPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTwittersPath.Location = new System.Drawing.Point(91, 13);
            this.lblTwittersPath.Name = "lblTwittersPath";
            this.lblTwittersPath.Size = new System.Drawing.Size(0, 13);
            this.lblTwittersPath.TabIndex = 2;
            // 
            // lblSelfiesPath
            // 
            this.lblSelfiesPath.AutoSize = true;
            this.lblSelfiesPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelfiesPath.Location = new System.Drawing.Point(91, 37);
            this.lblSelfiesPath.Name = "lblSelfiesPath";
            this.lblSelfiesPath.Size = new System.Drawing.Size(0, 13);
            this.lblSelfiesPath.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Selfies Path:";
            // 
            // btnListener
            // 
            this.btnListener.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnListener.Location = new System.Drawing.Point(351, 12);
            this.btnListener.Name = "btnListener";
            this.btnListener.Size = new System.Drawing.Size(67, 44);
            this.btnListener.TabIndex = 5;
            this.btnListener.Text = "STOP";
            this.btnListener.UseVisualStyleBackColor = true;
            this.btnListener.Click += new System.EventHandler(this.btnListener_Click);
            // 
            // _timer
            // 
            this._timer.Interval = 60000;
            this._timer.Tick += new System.EventHandler(this._timer_Tick);
            // 
            // btnFile
            // 
            this.btnFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFile.Location = new System.Drawing.Point(351, 62);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(67, 30);
            this.btnFile.TabIndex = 6;
            this.btnFile.Text = "LOG";
            this.btnFile.UseVisualStyleBackColor = true;
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Log Path:";
            // 
            // lnkLabel
            // 
            this.lnkLabel.AutoSize = true;
            this.lnkLabel.Location = new System.Drawing.Point(91, 71);
            this.lnkLabel.Name = "lnkLabel";
            this.lnkLabel.Size = new System.Drawing.Size(0, 13);
            this.lnkLabel.TabIndex = 10;
            this.lnkLabel.Click += new System.EventHandler(this.lnkLabel_Click);
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(430, 496);
            this.Controls.Add(this.lnkLabel);
            this.Controls.Add(this.btnFile);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnListener);
            this.Controls.Add(this.lblSelfiesPath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblTwittersPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Activity);
            this.Name = "Form1";
            this.Text = "Selfies Server";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        public class ListenerProcessInfo
        {
            public string ProcessPath { get; set; }
            public bool IsResponding { get; set; }
            public int ProcessId { get; set; }
        }
	}
}
