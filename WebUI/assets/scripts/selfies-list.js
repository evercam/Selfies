﻿var SelfiesIndex = function () {

    var EvercamApi = "https://api.evercam.io/v1";
    var utilsApi = "http://apiutils.azurewebsites.net/v1";
    var StartLive = false;
    var EndTimeout;
    var selfiesIdNullHash = 0;
    var showDropdown = false;
    var oLogTable;
    var isTbaleInitialized = false;

    var pager;
    var items = 100;
    var itemsOnPage = 20;
    var pageCount = items / itemsOnPage;

    var loadResponses = function () {
        selfiesIdNullHash = 0;
        getLogs();
        $.ajax({
            type: 'POST',
            url: 'WebService/SelfiesService.asmx/GetList',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ evercamId: localStorage.getItem("evercamUserId") }),
            dataType: 'json',
            context: $('body'),
            success: function (json) {
                var selfies = eval(json.d);
                $(".cameras-list").html("");
                var nullHashHtml = '';
                if (selfies.length == 0) {
                    $(".cameras-list").html('<div class="row-fluid no-content">You have not created any twitter response. <a href="javascript:;" class="newTweetResponse">Click here</a> to create one.</div>');
                    return;
                }
                for (var i = 0; i < selfies.length; i++) {
                    var logohtml = '';
                    
                    if (selfies[i].LogoFile != null) {
                        logohtml = '<span style="position:absolute;z-index:1;margin-left:-28px;margin-top:3px"><img style="width:25px;" src="' + selfies[i].LogoFile + '" /></span>';
                    }
                        
                    var html = '<div id="dataslot' + selfies[i].Id + '" class="row-fluid list-border margin-bottom15">';
                    html += '    <div class="span9">';
                    html += '        <input type="hidden" id="txtSnapUrl' + selfies[i].Id + '" value="' + selfies[i].SnapshotUrl + '" /><input type="hidden" id="txtHashTag' + selfies[i].Id + '" value="' + (selfies[i].HashTag == null ? "" : selfies[i].HashTag) + '" /><input type="hidden" id="txtCameraId' + selfies[i].Id + '" value="' + selfies[i].CameraId + '" /><input type="hidden" id="txtTweetResponse' + selfies[i].Id + '" value="' + selfies[i].Response + '" />';
                    html += '        <input type="hidden" id="txtFooterText' + selfies[i].Id + '" value="' + (selfies[i].PhotoText == null ? "" : selfies[i].PhotoText) + '" /><input type="hidden" id="txtTwitterId' + selfies[i].Id + '" value="' + (selfies[i].TwitterId == null ? "" : selfies[i].TwitterId) + '" /><input type="hidden" id="txtLogoFile' + selfies[i].Id + '" value="' + (selfies[i].LogoFile == null ? "" : selfies[i].LogoFile) + '" />';

                    html += '        <div class="cam-list-row"><a class="commonLinks showImgPopup" href="javascript:;" data-val="' + selfies[i].Id + '">' + (selfies[i].HashTag == null ? '@' + (selfies[i].TwitterId == null ? "" : selfies[i].TwitterId) : selfies[i].HashTag) + '</a>&nbsp;<span style="font-size:15px;color:#999999;">' + (selfies[i].HashTag == null ? '' : (selfies[i].TwitterId == null ? "" : ' @' + selfies[i].TwitterId)) + '</span></div>';
                    html += '        <div class="hash-label"><b>Footer Text: </b>' + (selfies[i].PhotoText == null ? "" : selfies[i].PhotoText) + '</div>';
                    html += '        <div class="hash-label"><b>Tweet Response: </b>' + (selfies[i].Response == null ? "" : selfies[i].Response) + '</div>';
                    html += '        <div class=""><a class="tools-link ListnerStart" data-val="' + selfies[i].Id + '" href="javascript:;"><i class="' + (selfies[i].IsActive ? 'icon-check' : 'icon-check-empty') + '"></i> Active</a>&nbsp;&nbsp;&nbsp;<a href="javascript:;" class="tools-link editTweet" data-val="' + selfies[i].Id + '"><i class="icon-edit"></i> Edit</a>&nbsp;&nbsp;';
                    html += '             <span class="popbox2"><a href="javascript:;" class="tools-link open2" data-val="' + selfies[i].Id + '"><i class="icon-remove"></i> Delete</a>';
                    html += '             <div class="collapse-popup">';
                    html += '               <div class="box2" style="width:250px;">';
                    html += '                   <div class="arrow2"></div>';
                    html += '                   <div class="arrow-border2"></div>';
                    html += '                   <div class="margin-bottom-10">Are you sure to delete this response?</div>';
                    html += '                   <div class="margin-bottom-10"><input class="btn deleteTweet" type="button" value=" Delete " data-val="' + selfies[i].Id + '" data-action="r" />&nbsp;&nbsp;<a href="#" class="closepopup2">Cancel</a></div>';
                    html += '               </div>';
                    html += '             </div></span>';
                    html += '        </div>';
                    html += '    </div>';
                    html += '    <div class="span3">';
                    html += '        <div class="text-right"><img style="cursor:pointer;" data-val="' + selfies[i].Id + '" id="thumb' + selfies[i].Id + '" src="assets/img/ajaxloader.gif" class="img-box showImgPopup" />' + logohtml + '</div>';
                    html += '    </div>';
                    html += '</div>';
                    if (selfies[i].HashTag == null) {
                        nullHashHtml = html;
                        selfiesIdNullHash = selfies[i].Id;
                    }
                    else
                        $(".cameras-list").append(html);
                    GetLatestImage("thumb" + selfies[i].Id, selfies[i].CameraId);
                }
                $(".cameras-list").prepend(nullHashHtml);
                $('.popbox2').popbox({
                    open: '.open2',
                    box: '.box2',
                    arrow: '.arrow2',
                    arrow_border: '.arrow-border2',
                    close: '.closepopup2'
                });
                
            },
            error: function (jqxhr, textstatus, errorthrown) {
                if (errorthrown != '' && errorthrown != undefined)
                    jAlert("The error is: " + errorthrown, "Oh oh - something went wrong!");
            }
        });
    }

    var loadLogs = function (filter) {
        //$("#divLogs").html("");
        $.ajax({
            type: 'POST',
            url: 'WebService/SelfiesService.asmx/GetLogs',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ evercamId: localStorage.getItem("evercamUserId"), filter: filter }),
            dataType: 'json',
            context: $('body'),
            success: function (json) {
                var logs = eval(json.d);
                items = logs.length;
                oLogTable.fnClearTable();
                oLogTable.fnAddData(logs);
                /*for (var i = 0; i < logs.length; i++) {
                    var record = (i + 1);
                    var html = '<div class="row-fluid margin-bottom15 record_' + record + ' ' + (record > itemsOnPage ? 'hide' : '') + '">';
                    html += '    ' + logs[i].Message;
                    html += '   </div>';

                    $("#divLogs").append(html);
                }
                $('#divPager').pagination({
                    items: items,
                    itemsOnPage: itemsOnPage
                });*/
            },
            error: function (jqxhr, textstatus, errorthrown) {
                if (errorthrown != '' && errorthrown != undefined)
                    jAlert("The error is: " + errorthrown, "Oh oh - something went wrong!");
            }
        });
    }

    var getLogs = function () {
        loadLogs("");
        if (isTbaleInitialized)
            return;
        oLogTable = $('#tblLog').dataTable({
            /*"aoColumnDefs": [
                { "aTargets": [0] }
            ],*/
            "aoColumns":
			    [
				    { mData: 'FormatedDate', "sTitle": "Date", sWidth: "110px", sClass: "borderright", fnRender: function (obj) { return obj.aData.FormatedDate; } },
				    { mData: 'Message', "sTitle": "Message", sWidth: "330%", sClass: "borderright", fnRender: function (obj) { return obj.aData.Message; } },
				    { mData: 'IP', "sTitle": "IP", sWidth: "110px", sClass: "left_align_last_cell", fnRender: function (obj) { return obj.aData.IP; } },
			        { mData: 'ID', "sTitle": "Id", "bVisible": false, sClass: "left_align_last_cell", fnRender: function (obj) { return obj.aData.ID; } }
			    ],
            "aaSorting": [[3, 'desc']],
            "oLanguage": {
                "sEmptyTable": "No data available!",
                "sLengthMenu": "_MENU_ ",
                "oPaginate": {
                    "sPrevious": "Prev",
                    "sNext": "Next"
                }
            },
            "aLengthMenu": [
               [10, 25, 50, 100],
               [10, 25, 50, 100] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 100,
        });

        //jQuery('#tblLog_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
        jQuery('#tblLog_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
        isTbaleInitialized = true;
    }

    var getTwitters = function () {
        var oauth_token = getCGIParameter("oauth_token");
        var oauth_verifier = getCGIParameter("oauth_verifier");
        if (oauth_token != "" && oauth_verifier != "")
            return handleTwitterOauth();
        $.ajax({
            type: 'POST',
            url: 'WebService/SelfiesService.asmx/GetTwitterInfo',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ evercamId: localStorage.getItem("evercamUserId") }),
            dataType: 'json',
            context: $('body'),
            success: function (json) {
                var twitters = eval(json.d);
                if (twitters.length == 0) {
                    if (oauth_token == "" && oauth_verifier == "")
                        showtwitterBallon();
                    handleTwitterOauth();
                    $("#btnChangeAccount").hide();
                }
                    
                //$("#txtTotalTwitters").val(twitters.length);
                for (var i = 0; i < twitters.length; i++) {
                    $("#btnTwitterConnect").html('<i class="icon-twitter"></i> | @' + twitters[i].ID + ' &nbsp;&nbsp;<i class="icon-angle-down">');
                    $("#btnTwitterConnect").show();
                    $("#divTour").hide();
                    $("#btnTwitterConnect").bind("click", showedDropdownMenu);
                    localStorage.setItem("TwitterId", twitters[i].ID);
                    localStorage.setItem("oauth_twitter_completed", "yes");

                    /*var html = '<div class="control-group">';
                    html += '       <div class="label-font span11">@' + twitters[i].ID + '</div>';
                    html += '       <div id="responselength' + i + '" class="span1 text-right">' + (twitters[i].Response == null ? '125' : 125 - twitters[i].Response.length) + '</div>';
                    html += '       <div class="controls">';
                    html += '          <input type="text" data-val="' + i + '" maxlength="125" id="txtResponse' + i + '" class="span m-wrap white responselength" value="' + (twitters[i].Response == null ? '' : twitters[i].Response) + '" />';
                    html += '          <input type="hidden" id="txtResponseTwitterId' + i + '" value="' + twitters[i].ID + '"/>';
                    html += '       </div>';
                    html += '   </div>';

                    $("#divResponses").prepend(html);*/
                }
                getUsersInfo();
                loadResponses();
            },
            error: function (jqxhr, textstatus, errorthrown) {
                if (errorthrown != '' && errorthrown != undefined)
                    jAlert("The error is: " + errorthrown, "Oh oh - something went wrong!");
            }
        });
    }

    var showedDropdownMenu = function (e) {
        if ($("#btnTwitterConnect").html().indexOf('Connect') > 0)
            return;
        var isOpen = $(".dropdown-menu").css("display");
        if (isOpen == "none") {
            var p = $("#btnTwitterConnect");
            var position = p.position();
            $(".dropdown-menu").css({
                left: position.left,
                top: position.top + p.height()+24,
                width: p.width()+40
            });
            $(".dropdown-menu").slideDown(300);
            e.stopPropagation();
        }
        else
            $(".dropdown-menu").slideUp(300);
    }

    $(".clearTwitterAccount").live("click", function () {
        $(".dropdown-menu").slideUp(300);
        $.ajax({
            type: 'POST',
            url: 'WebService/SelfiesService.asmx/GetOauthURL',
            context: $('body'),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: JSON.stringify({ evercamId: localStorage.getItem("evercamUserId"), twitterId: localStorage.getItem("TwitterId") }),
            success: function (json) {
                $("#btnTwitterConnect").html('<i class="icon-twitter"></i> | Connect');
                $("#btnTwitterConnect").live("click", twitterImageClicked);
                localStorage.removeItem("TwitterId");
                localStorage.removeItem("oauth_twitter_completed");
                loadResponses();
                //location.href = url;
            },
            error: function (jqxhr, textstatus, errorthrown) {
                if (errorthrown != '' && errorthrown != undefined)
                    jAlert("The error is: " + errorthrown, "Oh oh - something went wrong!");
                //imgEle.attr("src", "assets/img/twitter_connect.png");
            }
        });
    });

    $(".responselength").live("keyup", function () {
        var index = $(this).attr("data-val");
        var str = $(this).val();
        $("#responselength" + index).html(125 - str.length);
    });

    var GetLatestImage = function (id, cameraId) {
        $.ajax({
            type: "GET",
            crossDomain: true,
            url: EvercamApi + "/cameras/" + cameraId + "/recordings/snapshots/latest.json",
            beforeSend: function (xhrObj) {
                xhrObj.setRequestHeader("Authorization", localStorage.getItem("oAuthTokenType") + " " + localStorage.getItem("oAuthToken"));
            },
            data: { with_data: true },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                if (res.snapshots[0].data != null)
                    $("#" + id).attr("src", res.snapshots[0].data);
                else
                    $("#" + id).attr("src", "assets/img/cam-img.jpg");
                isloadedImg = true;
                setTimeout(function () {
                    var imgWidth = $("#" + id).width();
                    var imgHeight = $("#" + id).height();
                    $("#spnTempFooterText").width(imgWidth);
                    $("#imgTempLogo").show();
                    $("#spnTempFooterText").show();
                    var marginLeft = imgWidth;
                    var marginTop = imgHeight - ($("#spnTempFooterText").height() + 5);
                    $("#spnTempFooterText").css("margin-left", -marginLeft);
                    $("#spnTempFooterText").css("margin-top", marginTop);
                    
                }, 2000);
            },
            error: function (xhrc, ajaxOptionsc, thrownErrorc) {
                $("#" + id).attr("src", "assets/img/cam-img.jpg");
            }
        });
    };

    $(".showImgPopup").live("click", function (e) {
        var id = $(this).attr("data-val");
        var cameraId = $("#txtCameraId" + id).val();
        var hashTag = $("#txtHashTag" + id).val();
        var logoFile = $("#txtLogoFile" + id).val();
        var footerText = $("#txtFooterText" + id).val();
        if (hashTag == '')
            hashTag = '@' + $("#txtTwitterId" + id).val();
        showcam(cameraId, hashTag, "", "");
    });
    
    var showcam = function (cameraId, hash,logoFile,footerText) {
        clearTimeout(EndTimeout);
        StartLive = false;
        var camimageid = "camimage" + new Date().getTime();
        $("#imgTempLogo").hide();
        $("#spnTempFooterText").hide();
        var strHtml = "<img id='" + camimageid + "' src='assets/img/ajaxloader.gif' style='max-height:480px;'/>";
        if (logoFile != '')
            strHtml += '<span id="imgTempLogo" style="position:absolute;z-index:1;margin-left:-55px;margin-top:8px; display:none;"><img style="height:45px;" src="' + logoFile + '" /></span>';
        if (footerText != '')
            strHtml += '<span id="spnTempFooterText" class="imgfooter-text" style="display:none;font-size:18px;">' + footerText + '</span>';
            

        $("#mydialog").html(strHtml);
        StartLive = true;
        $("#mydialog").dialog({
            overlay: { backgroundColor: "white", opacity: 0 },
            show: { effect: "explode", duration: 300 },
            //height: 480,
            width: 640,
            position: ['center',20],
            title: hash,
            modal: true,
            create: function (event, ui) {
                $("body").css({ overflow: 'hidden' })
            },
            beforeClose: function (event, ui) {
                $("body").css({ overflow: 'inherit' })
            },
            buttons: [
                {
                    text: "Close",
                    click: function () {
                        $(this).dialog("close");
                        StartLive = false;
                        $("#mydialog").html('');
                    }
                }]
        });
        $(".ui-dialog-titlebar-close").hide();
        reloadCameraImage(cameraId, camimageid);
    }

    var reloadCameraImage = function (cameraId, camimageid) {
        if (StartLive) {
            EndTimeout = setTimeout(function () {
                GetLatestImage(camimageid, cameraId);
                reloadCameraImage(cameraId, camimageid);
            }, 2500);
        }
    }

    var getCGIParameter = function (name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null) {
            return "";
        }
        else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

    var handleTwitterOauth = function () {
        var oauth_token = getCGIParameter("oauth_token");
        var oauth_verifier = getCGIParameter("oauth_verifier");
        oauth_completed = getCGIParameter("oauth_completed");

        if (oauth_token != "" && oauth_verifier != "") {
            
            $("#twitterimg").attr("src", "assets/img/ajaxloader1.gif");
            
            var stufftosend = new Object();
            stufftosend.token = oauth_token;
            stufftosend.verifier = oauth_verifier;
            var ts = localStorage.getItem("token_secret", ts);
            stufftosend.evercamId = localStorage.getItem("evercamUserId");

            $.ajax({
                type: 'POST',
                url: 'WebService/SelfiesService.asmx/Authorise',
                context: $('body'),
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: JSON.stringify(stufftosend),
                success: function (json) {
                    if (json.d == "") {
                        jAlert("The Twitter account you specified is already in use by another Evercam user. Please disconnect this first or choose a different Twitter account.", "Twitter account already connected", rediredtSite);
                        $("#btnTwitterConnect").bind("click", twitterImageClicked);
                        return;
                    } else {
                        localStorage.setItem("TwitterId", json.d[1]);
                        localStorage.setItem("oauth_twitter_completed", "yes");
                        location.href = "./index.html";
                    }
                },
                error: function (jqxhr, textstatus, errorthrown) {
                    if (errorthrown != '' && errorthrown != undefined)
                        jAlert("The error is: " + errorthrown, "Oh oh - something went wrong!");
                    //location.href = "./index.html";
                }
            });
            return false;
        }
        else if (localStorage.getItem("oauth_twitter_completed") != null && localStorage.getItem("oauth_twitter_completed") != undefined) {
            
            // change text to "connected to twitter"
            //$("#connectedtotwittertext").html("<span style=\"color: #97fd9f;\">CONNECTED TO TWITTER ACCOUNT</span>");
            $("#btnTwitterConnect").html('<i class="icon-twitter"></i> | @' + localStorage.getItem("TwitterId"));
            $("#btnTwitterConnect").show();
            // change Twitter to green image
            //$("#twitterimg").attr("src", "assets/img/twitter_connect-green.png");
            return true;
        }
        else {
            $("#btnTwitterConnect").bind("click", twitterImageClicked);
            $("#btnTwitterConnect").show();
            return true;
        }
    }

    var rediredtSite = function () {
        location.href = "./index.html";
    }

    var getParameterByName = function (name, searchString) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(searchString);//location.search);
        return results == null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    var handleLoginSection = function() {
        //// For local testing ONLY
        //localStorage.setItem("oAuthTokenType", "bearer");
        //localStorage.setItem("oAuthToken", "99550345bd31f74f5bfff5444cefe5b8");
        //localStorage.setItem("tokenExpiry", "315568999");
        //localStorage.setItem("evercamUserId", "azharmalik3");
        //localStorage.setItem("evercamUsername", "azharmalik3");

        if (localStorage.getItem("oAuthToken") != null && localStorage.getItem("oAuthToken") != undefined) {
            getTwitters();
            reloadForm();
            $(".fullwidthbanner-container").show();
        } else {
            var stringHash = window.location.hash;

            if (stringHash != "") {
                var hasToken = getParameterByName('access_token', "?" + stringHash.substring(1));

                if (hasToken != "" && hasToken != null) {
                    var hasTokenType = getParameterByName('token_type', window.location.hash);
                    var tokenExpiry = getParameterByName('expires_in', window.location.hash);
                    var stufftosend = new Object();
                    stufftosend.token_endpoint = "https://api.evercam.io/oauth2/tokeninfo?access_token=" + hasToken;
                    $.ajax({
                        type: 'POST',
                        url: 'WebService/SelfiesService.asmx/GetTokenUser',
                        context: $('body'),
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        data: JSON.stringify(stufftosend),
                        success: function(res) {
                            if (res.d.userid != "null" && res.d.userid != '') {
                                var userId = res.d.userid;
                                localStorage.setItem("oAuthTokenType", hasTokenType);
                                localStorage.setItem("oAuthToken", hasToken);
                                localStorage.setItem("tokenExpiry", tokenExpiry);

                                localStorage.setItem("evercamUserId", userId);
                                localStorage.setItem("evercamUsername", userId);
                                $("#displayUsername").html(userId);
                                $("#displayUsername").show();
                                $(".fullwidthbanner-container").show();
                                window.location.hash = '';
                                insertLoginLogoutLog("login", userId);
                                getTwitters();

                            } else window.location = 'login.html';
                        },
                        error: function(jqxhr, textstatus, errorthrown) {

                        }
                    });
                } else
                    window.location = 'login.html';
            } else
                window.location = 'login.html';
        }
    };

    var showtwitterBallon = function () {
        
        //$(".intro-tour-overlay").show();
        var p = $("#btnTwitterConnect");
        var position = p.position();
        $("#divTour").css("left", position.left - 321);
        $("#divTour").show();
    }

    var getUsersInfo = function () {
        $.ajax({
            type: "GET",
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            url: EvercamApi + "/users/" + localStorage.getItem("evercamUserId") + ".json",
            beforeSend: function (xhrObj) {
                xhrObj.setRequestHeader("Authorization", localStorage.getItem("oAuthTokenType") + " " + localStorage.getItem("oAuthToken"));
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                loggedInUser = res.users[0];
                if (res.users[0].firstname == "" && res.users[0].lastname == "")
                    return;
                localStorage.setItem("evercamUsername", res.users[0].firstname + " " + res.users[0].lastname);
                $("#displayUsername").html(res.users[0].firstname + " " + res.users[0].lastname)
            },
            error: function (xhrc, ajaxOptionsc, thrownErrorc) { }
        });
    }

    var handleLogout = function () {
        $('#lnkSignout').click(function () {
            insertLoginLogoutLog("logout", localStorage.getItem("evercamUserId"));
            localStorage.removeItem("oAuthTokenType");
            localStorage.removeItem("oAuthToken");
            localStorage.removeItem("tokenExpiry");
            localStorage.removeItem("evercamUserId");
            localStorage.removeItem("evercamUsername");
            localStorage.removeItem("oauth_twitter_completed");
            localStorage.removeItem("TwitterId");
            localStorage.removeItem("token_secret");
            localStorage.removeItem("EvercamCameras");
            clearLocalStorage();
        });
    }

    var handleLogin = function () {
        if (localStorage.getItem("oAuthToken") == null || localStorage.getItem("oAuthToken") == undefined) {
            window.location = 'login.html';
            return false;
        } else {
            $("body").removeClass("no-dispaly");
            return true;
        }
    }

    var handleTabs = function () {
        $(".tab-width div").bind("click", function () {
            if ($(this, "div").hasClass("container-tab-selected"))
                return;
            $(".tab-width div").each(function (idx, el) {
                $(this).removeClass("container-tab-selected").addClass("container-tab");
            });
            $(this).removeClass("container-tab").addClass("container-tab-selected");
            var tab = $(this).html();
            if (tab == "Your Cameras") {
                $(".cameras-list").show();
                $(".logs").hide();
                $(".settings").hide();
            }
            else if (tab == "Logs") {
                $(".logs").show();
                $(".cameras-list").hide();
                $(".settings").hide();
            } else if (tab == "Settings") {
                $(".settings").show();
                $(".cameras-list").hide();
                $(".logs").hide();
            }
        });
    }

    var showResponseForm = function () {
        $("#lnNewTwitterResponse").bind("click", function () {
            /*if (localStorage.getItem("TwitterId") == null || localStorage.getItem("TwitterId") == undefined) {
                showtwitterBallon();
                //return;
            }*/
            $.get('NewTwitterResponse.html', function (data) {
                $("#divResponseForm div").html(data);
                getCameras("");
                //jQuery('.tooltips').tooltip();
                $("#divResponseForm").slideDown(500);
                handleFileupload();
            });
            $(this).hide();
            $("#lnNewTwitterResponseCol").show();
            $("#txtSelfiesId").val(0);
            $("#txtLogoFile").val("");
        });

        $("#lnNewTwitterResponseCol").bind("click", function () {
            $("#divResponseForm").slideUp(500, function () {
                $("#divResponseForm div").html("");
                $("#lnNewTwitterResponseCol").hide();
                $("#lnNewTwitterResponse").show();
                $("#txtSelfiesId").val(0);
                $("#txtLogoFile").val("");
                clearLocalStorage();
            });            
        });
    }

    $(".newTweetResponse").live("click", function () {
        /*if (localStorage.getItem("TwitterId") == null || localStorage.getItem("TwitterId") == undefined) {
            showtwitterBallon();
            //return;
        }*/
        $.get('NewTwitterResponse.html', function (data) {
            $("#divResponseForm div").html(data);
            getCameras("");
            //jQuery('.tooltips').tooltip();
            $("#divResponseForm").slideDown(500);
            handleFileupload();
        });
        $("#lnNewTwitterResponse").hide();
        $("#lnNewTwitterResponseCol").show();
        $("#txtSelfiesId").val(0);
        $("#txtLogoFile").val("");
    });

    var handleFileupload = function() {

        // Initialize the jQuery File Upload widget:
        $('#fileupload').fileupload({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: 'assets/plugins/jquery-file-upload/server/php/'
        });
    };

    var getCameras = function(id) {
        $("#ddlCameras").removeClass("span12").addClass("span11");
        $.ajax({
            type: "GET",
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            url: EvercamApi + "/cameras.json",
            beforeSend: function(xhrObj) {
                xhrObj.setRequestHeader("Authorization", localStorage.getItem("oAuthTokenType") + " " + localStorage.getItem("oAuthToken"));
            },
            data: { user_id: localStorage.getItem("evercamUserId"), include_shared: true, thumbnail: false },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(res) {
                localStorage.setItem("EvercamCameras", JSON.stringify(res));
                var cams = res;
                for (var i = 0; i < cams.cameras.length; i++) {
                    var css = 'onlinec';
                    if (!cams.cameras[i].is_online)
                        css = 'offlinec';
                    if (cams.cameras[i].rights.indexOf("snapshot") > -1) {
                        var isSelect = '';
                        if (id == cams.cameras[i].id) {
                            isSelect = 'selected="selected"';
                            loadSelectedCamImage(id);
                        }
                        if (localStorage.getItem("reloadForm") && localStorage.getItem("cameraId") == cams.cameras[i].id) {
                            isSelect = 'selected="selected"';
                            loadSelectedCamImage(cams.cameras[i].id);
                            clearLocalStorage();
                        }
                        if (cams.cameras[i].thumbnail_url != undefined)
                            $("#ddlCameras").append('<option class="' + css + '" data-val="' + cams.cameras[i].thumbnail_url + '" ' + isSelect + ' value="' + cams.cameras[i].id + '" >' + cams.cameras[i].name + '</option>');
                    } else
                        console.log("Insufficient rights: " + cams.cameras[i].id);
                }
                $("#imgCamLoader").hide();
                $("#ddlCameras").removeClass("span11").addClass("span12");
                $("#ddlCameras").select2({
                    placeholder: 'Select Camera',
                    allowClear: true,
                    formatResult: format,
                    formatSelection: format,
                    escapeMarkup: function(m) {
                        return m;
                    }
                });

            },
            error: function(xhrc, ajaxOptionsc, thrownErrorc) {
                $("#ddlCameras").removeClass("span11").addClass("span12");
                $("#imgCamLoader").hide();
                if (xhrc.status == 401) {
                    localStorage.removeItem("oAuthToken");
                    localStorage.removeItem("evercamUsername");
                    localStorage.removeItem("evercamUserId");
                    localStorage.removeItem("oAuthTokenType");
                    localStorage.removeItem("EvercamCameras");
                    localStorage.removeItem("oauth_twitter_completed");
                    localStorage.removeItem("TwitterId");
                    window.location = 'login.html';
                }
            }
        });
    };

    var format = function (state) {
        if (!state.id) return state.text;
        if (state.id == "0") return state.text;
        //return "<img class='flag' src='assets/img/" + state.css + ".png'/>&nbsp;&nbsp;" + state.text;
        if (state.element[0].attributes[1].nodeValue == "null")
            return "<table style='width:100%;'><tr><td style='width:90%;'><img style='width:35px;height:30px;' class='flag' src='assets/img/cam-img-small.jpg'/>&nbsp;&nbsp;" + state.text + "</td><td style='width:10%;' align='right'>" + "<img style='margin-top: -6px;' class='flag' src='assets/img/" + state.css + ".png'/>" + "</td></tr></table>";
        else
            return "<table style='width:100%;'><tr><td style='width:90%;'><img style='width:35px;height:30px;' class='flag' src='" + state.element[0].attributes[1].nodeValue + "'/>&nbsp;&nbsp;" + state.text + "</td><td style='width:10%;' align='right'>" + "<img class='flag' style='margin-top: -6px;' src='assets/img/" + state.css + ".png'/>" + "</td></tr></table>";
    }

    $("#ddlCameras").live("change", function () {
        var cameraId = $(this).val();
        $("#txtHashTag").val("#" + cameraId);
        if ($("option:selected", this).attr("class") == 'offlinec') {
            $("#divWarning").slideDown();
            $("#imgPreview").attr('src', 'assets/img/cam-img.jpg');

        } else {
            $("#divWarning").slideUp();
            loadSelectedCamImage(cameraId);
        }
    });

    var loadSelectedCamImage = function (cameraId) {
        $("#imgPreview").attr('src', 'assets/img/ajaxloader.gif');
        $.ajax({
            type: "GET",
            crossDomain: true,
            url: EvercamApi + "/cameras/" + cameraId + "/recordings/snapshots/latest.json",
            beforeSend: function (xhrObj) {
                xhrObj.setRequestHeader("Authorization", localStorage.getItem("oAuthTokenType") + " " + localStorage.getItem("oAuthToken"));
            },
            data: { with_data: true },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                if (res.snapshots[0].data == null || res.snapshots[0].data == undefined) {
                    $("#imgPreview").attr('src', 'assets/img/cam-img.jpg');
                } else {
                    $("#imgPreview").attr('src', res.snapshots[0].data);
                    setTextLogoPositions();
                }
            },
            error: function (xhrc, ajaxOptionsc, thrownErrorc) {
                $("#imgPreview").attr('src', 'assets/img/cam-img.jpg');
                setTextLogoPositions();
            }
        });
    }

    var setTextLogoPositions = function () {
        var imgWidth = $("#imgPreview").width();
        var imgHeight = $("#imgPreview").height();
        $("#spnFooterText").width(imgWidth);
        $("#spnFooterText").text($("#txtFooterText").val());

        if (navigator.userAgent.match(/Chrome/) == "Chrome") {
            var marginLeft = imgWidth / 2 - $("#spnFooterText").width() / 2;
            var marginTop = imgHeight - ($("#spnFooterText").height() + 5);
            //$("#spnFooterText").css("margin-left", marginLeft);
            $("#spnFooterText").css("margin-top", marginTop);

            marginLeft = imgWidth - 45;
            marginTop = 5 - imgHeight;//($("#imgPreviewLogo").height() + 5) - imgHeight;
            $("#imgPreviewLogo").css("margin-left", marginLeft);
            $("#imgPreviewLogo").css("margin-top", marginTop);
        }
        else {
            $("#spnFooterText").css("margin-top", imgHeight - 10);
        }
        $("#imgPreviewLogo img").show();
        if (localStorage.getItem("TwitterId") == null || localStorage.getItem("TwitterId") == undefined) {
            localStorage.setItem("cameraId", $("#ddlCameras").val());
            localStorage.setItem("hashTag", $("#txtHashTag").val());
            localStorage.setItem("reloadForm", true);
        }
    }

    $(".formButtonOk").live("click", function () {
        $("#divAlert").removeClass("alert-info").addClass("alert-error");
        if ($("#txtSelfiesId").val() == 0) {
            if (localStorage.getItem("TwitterId") == null || localStorage.getItem("TwitterId") == undefined) {
                $("#divAlert").slideDown();
                $("#divAlert span").html("Please connect your Twitter account first.");
                localStorage.setItem("cameraId", $("#ddlCameras").val());
                localStorage.setItem("hashTag", $("#txtHashTag").val());
                localStorage.setItem("footerText", $("#txtFooterText").val());
                localStorage.setItem("tweetResponse", $("#txtTweetResponse").val());
                localStorage.setItem("reloadForm", true);
                return;
            }
        }
        if ($("#ddlCameras").val() == "") {
            $("#divAlert").slideDown();
            $("#divAlert span").html("Please select camera to continue.");
            return;
        }
        if ($("#txtHashTag").val() == "" && selfiesIdNullHash > 0) {
            var isEditSelfiesId = parseInt($("#txtSelfiesId").val());
            if (isEditSelfiesId > 0 && isEditSelfiesId != selfiesIdNullHash) {
                $("#divAlert").slideDown();
                $("#divAlert span").html("Could not save this selfies without a #hashtag because there is another default Selfies without a #hashtag. First assign any #hashtag to that one, then you will be able to create this without a #hashtag.");
                return;
            }
            else if (isEditSelfiesId == 0) {
                $("#divAlert").slideDown();
                $("#divAlert span").html("Could not save this selfies without a #hashtag because there is another default Selfies without a #hashtag. First assign any #hashtag to that one, then you will be able to create this without a #hashtag.");
                return;
            }
        }
        else if ($("#txtHashTag").val() != "") {
            var hashTag = $("#txtHashTag").val();
            if (hashTag.indexOf('#') == 0)
                hashTag = hashTag.substring(1);
            if (!validateAlphaNumeric(hashTag)) {
                $("#divAlert").slideDown();
                $("#divAlert span").html("#hashtag should be only alphanumeric.");
                return;
            }
        }
        if ($("#txtTweetResponse").val().indexOf('{@mentions}') == -1) {
            $("#divAlert").slideDown();
            $("#divAlert span").html('Please inlcude <b>{@mentions}</b> in response message.');
            return;
        }

        if ($('.table-striped td.name').html() != undefined) {
            $(".fileupload-buttonbar").hide();
            $("#divAlert").removeClass("alert-error").addClass("alert-info");
            $("#divAlert").slideDown();
            $("#divAlert span").html('<img src="assets/img/loader3.gif"/>&nbsp;Saving Twitter Response');
            $('.table-striped td.start button.btn').click();
            setTimeout(function () { checkFileUploadAndSave($(".progress-success").attr("aria-valuemin")); }, 1000);
        }
        else
            checkFileUploadAndSave(null);
    });

    var checkFileUploadAndSave = function (percentage) {
        $("#divAlert").removeClass("alert-error").addClass("alert-info");
        $("#divAlert").slideDown();
        $("#divAlert span").html('<img src="assets/img/loader3.gif"/>&nbsp;Saving Twitter Response');
        
        if (percentage != null && parseInt(percentage) <= 100) {
            setTimeout(function () { checkFileUploadAndSave($(".progress-success").attr("aria-valuemin")); }, 1000);
            return;
        } else {
            if ($('.table-striped td.preview a').length > 0)
                $("#txtLogoFile").val($('.table-striped td.preview a').attr("href"));
        }
        
        var hashTag = $("#txtHashTag").val();
        if (hashTag != '' && hashTag.indexOf('#') == -1)
            hashTag = '#' + hashTag;
        var twitterId = localStorage.getItem("TwitterId");
        if ($("#txtSelfiesId").val() != 0)
            twitterId = $("#txtTwitterId" + $("#txtSelfiesId").val()).val();

        var stufftosend = new Object();
        stufftosend.selfiesId = $("#txtSelfiesId").val();
        stufftosend.twitterId = twitterId;
        stufftosend.cameraId = $("#ddlCameras").val();
        stufftosend.evercamId = localStorage.getItem("evercamUserId");
        stufftosend.hashTag = hashTag;
        stufftosend.photoHeading = $("#txtFooterText").val();
        stufftosend.logoFile = $("#txtLogoFile").val();
        stufftosend.accessToken = localStorage.getItem("oAuthToken");

        var cams = JSON.parse(localStorage.getItem("EvercamCameras"));
        for (var i = 0; i < cams.cameras.length; i++) {
            if (cams.cameras[i].id == $("#ddlCameras").val()) {
                stufftosend.snapshotUrl = cams.cameras[i].external.http.jpg;
                stufftosend.userName = cams.cameras[i].cam_username;
                stufftosend.password = cams.cameras[i].cam_password;
            }
        }
        stufftosend.response = $("#txtTweetResponse").val();

        $.ajax({
            type: 'POST',
            url: 'WebService/SelfiesService.asmx/SaveSelfieDetails',
            context: $('body'),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: JSON.stringify(stufftosend),
            success: function (json) {
                loadResponses();
                var Id = json.d;
                $("#divResponseForm").slideUp(500, function () {
                    $("#divResponseForm div").html("");
                    $("#lnNewTwitterResponseCol").hide();
                    $("#lnNewTwitterResponse").show();
                    $("#txtSelfiesId").val(0);
                    $("#txtLogoFile").val("");
                });
            },
            error: function () {

            }
        });
    }

    $(".formButtonCancel").live("click", function () {
        $("#divResponseForm").slideUp(500, function () {
            $("#divResponseForm div").html("");
            $("#lnNewTwitterResponseCol").hide();
            $("#lnNewTwitterResponse").show();
            $("#txtSelfiesId").val(0);
            $("#txtLogoFile").val("");
            clearLocalStorage();
        });
    })

    $(".deleteTweet").live("click", function () {
        var id = $(this).attr("data-val");
        $.ajax({
            type: 'POST',
            url: 'WebService/SelfiesService.asmx/DeleteSelfiesTweet',
            context: $('body'),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: JSON.stringify({ selfiesId: id, evercamId: localStorage.getItem("evercamUserId"), cameraId: $("#txtCameraId" + id).val(), hashTag: $("#txtHashTag" + id).val(), twitterId: localStorage.getItem("TwitterId") }),
            success: function (json) {
                $("#dataslot" + id).slideUp(500, function () {
                    if (selfiesIdNullHash == id)
                        selfiesIdNullHash = 0;
                    $("#dataslot" + id).remove();
                    if ($(".cameras-list div").length == 0)
                        $(".cameras-list").html('<div class="row-fluid no-content">You have not created any twitter response. <a href="javascript:;" class="newTweetResponse">Click here</a> to create one.</div>');
                });
            },
            error: function () {

            }
        });
    })

    $(".editTweet").live("click", function () {
        var id = $(this).attr("data-val");
        $.get('NewTwitterResponse.html', function (data) {
            $("#divResponseForm div").html(data);
            getCameras($("#txtCameraId" + id).val());
            //jQuery('.tooltips').tooltip();
            $("#divResponseForm").slideDown(500);
            handleFileupload();
            $("#txtHashTag").val($("#txtHashTag" + id).val());
            $("#txtFooterText").val($("#txtFooterText" + id).val());
            $("#txtSelfiesId").val(id);
            var logoFile = $("#txtLogoFile" + id).val();
            $("#txtLogoFile").val(logoFile);
            $("#txtTweetResponse").val($("#txtTweetResponse" + id).val());
            if (logoFile != '') {
                $("#imgWatermarkLogo").attr('src', logoFile);
                $("#imgWatermarkLogo").show();
                $(".fileinput-button span").html("Change file...")
                $("#imgPreviewLogo").html('<img src="' + logoFile + '" style="width:40px;" />')
                $("#imgPreviewLogo img").hide();
                $("#spnFooterText").text($("#txtFooterText").val());
            }
            //setPreview();
        });
        $("#lnNewTwitterResponse").hide();
        $("#lnNewTwitterResponseCol").show();
        jQuery('html,body').animate({
            scrollTop: jQuery('body').offset().top
        }, 'slow');
    })

    $(".ListnerStart").live("click", function () {
        var id = $(this).attr("data-val");
        if ($("i", this).attr("class") == 'icon-check') {
            $("i", this).removeClass("icon-check").addClass("icon-check-empty");
        }
        else {
            $("i", this).removeClass("icon-check-empty").addClass("icon-check");
        }
        $.ajax({
            type: "POST",
            url: 'WebService/SelfiesService.asmx/ActiveSelfies',
            beforeSend: function (xhrObj) {
                //xhrObj.setRequestHeader("Authorization", localStorage.getItem("oAuthTokenType") + " " + localStorage.getItem("oAuthToken"));
            },
            data: JSON.stringify({ selfiesId: id, isActive: $("i", this).hasClass("icon-check"), hashTag: $("#txtHashTag" + id).val(), evercamId: localStorage.getItem("evercamUserId") }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                
            },
            error: function (xhrc, ajaxOptionsc, thrownErrorc) {
                
            }
        });
    })

    var twitterImageClicked = function () {

        // Show the spinning loader
        //showSpinningLoader();
        var imgEle = $(this);
        imgEle.attr("src", "assets/img/ajaxloader1.gif");

        // Store their camera address, hashtag & photo heading text
        //setCookie("cameraaddress", $("#cameraaddress").val());
        //setCookie("hashtag", $("#hashtag").val());
        //setCookie("photoheading", $("#photoheading").val());
        var stufftosend = new Object();
        $.ajax({
            type: 'POST',
            url: 'WebService/SelfiesService.asmx/GetURL',
            context: $('body'),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: stufftosend,
            success: function (json) {
                var url = json.d[1];
                var ts = json.d[0];
                localStorage.setItem("token_secret", ts);
                location.href = url;
            },
            error: function (jqxhr, textstatus, errorthrown) {
                if (errorthrown != '' && errorthrown != undefined)
                    jAlert("The error is: " + errorthrown, "Oh oh - something went wrong!");
                //imgEle.attr("src", "assets/img/twitter_connect.png");
            }
        });
    }
    
    var insertLoginLogoutLog = function (type, evercamUserId) {
        $.ajax({
            type: 'POST',
            url: 'WebService/SelfiesService.asmx/LoginLogoutLog',
            context: $('body'),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: JSON.stringify({ logType: type, evercamId: evercamUserId }),
            success: function (json) { if (type == "logout") { window.location = 'login.html'; } },
            error: function () { if (type == "logout") { window.location = 'login.html'; } }
        });
    }

    $("#txtFooterText").live("keyup", function () {
        var imgWidth = $("#imgPreview").width();
        var imgHeight = $("#imgPreview").height();
        $("#spnFooterText").width(imgWidth);

        var marginLeft = imgWidth / 2 - $("#spnFooterText").width() / 2;
        var marginTop = imgHeight - ($("#spnFooterText").height() + 5);
        //$("#spnFooterText").css("margin-left", marginLeft);
        $("#spnFooterText").css("margin-top", marginTop);
        $("#spnFooterText").text($("#txtFooterText").val());
        if (localStorage.getItem("TwitterId") == null || localStorage.getItem("TwitterId") == undefined) {
            localStorage.setItem("footerText", $("#txtFooterText").val());
            localStorage.setItem("reloadForm", true);
        }
    });

    $("#txtTweetResponse").live("keyup", function () {
        if (localStorage.getItem("TwitterId") == null || localStorage.getItem("TwitterId") == undefined) {
            localStorage.setItem("tweetResponse", $("#txtTweetResponse").val());
            localStorage.setItem("reloadForm", true);
        }
    });
   
    var handleWindowRessize = function () {
        $(window).bind("resize", function (e) {
            setPreview();
            var p = $("#btnTwitterConnect");
            var position = p.position();
            $("#divTour").css("left", position.left - 321);
        });

        $('body').bind('click', function (e) {
            var isOpen = $(".dropdown-menu").css("display");
            if (isOpen == "block")
                $(".dropdown-menu").slideUp(300);
        });

        $(".template-header").bind("click", function () {
            if ($(".template-body").css('display') == 'none')
                $(".template-body").slideDown();
            else
                $(".template-body").slideUp();
        });
        $(".closeoverlay").bind("click", function () {
            $(".intro-tour-overlay").hide();
            $("#divTour").hide();
        });
    }

    var setPreview = function () {
        var imgWidth = $("#imgPreview").width();
        var imgHeight = $("#imgPreview").height();
        $("#spnFooterText").width(imgWidth);
        var marginLeft = imgWidth / 2 - $("#spnFooterText").width() / 2;
        var marginTop = imgHeight - ($("#spnFooterText").height() + 5);

        $("#spnFooterText").text($("#txtFooterText").val());

        if (navigator.userAgent.match(/Chrome/) == "Chrome") {
            //$("#spnFooterText").css("margin-left", marginLeft);
            $("#spnFooterText").css("margin-top", marginTop);

            marginLeft = imgWidth - 45;
            marginTop = 5 - imgHeight;//($("#imgPreviewLogo").height() + 5) - imgHeight;
            $("#imgPreviewLogo").css("margin-left", marginLeft);
            $("#imgPreviewLogo").css("margin-top", marginTop);
        } else {
            marginLeft = imgWidth / 2 - $("#spnFooterText").width() / 2;
            marginTop = imgHeight - ($("#spnFooterText").height() + 5);
            //$("#spnFooterText").css("margin-left", marginLeft);
            $("#spnFooterText").css("margin-top", marginTop);
        }
    }

    var handleTwitterResponse = function () {
        $(".btnSaveTemplate").bind("click", function () {
            $("#divErrMsg").removeClass("alert-info").addClass("alert-error");
            var loopIndex = $("#txtTotalTwitters").val();
            if (loopIndex > 0) {
                var responses = new Array(loopIndex);
                for (var i = 0; i < loopIndex; i++) {
                    var message = $("#txtResponse" + i).val();
                    var twitterId = $("#txtResponseTwitterId" + i).val();
                    if (message.indexOf('{@mentions}') == -1) {
                        $("#divErrMsg").slideDown();
                        $("#divErrMsg span").html('Please inlcude <b>{@mentions}</b> in response message from @' + twitterId);
                        return;
                    }
                    responses[i] = new Array(2);
                    responses[i][0] = twitterId;
                    responses[i][1] = message;
                }
                $("#divErrMsg").removeClass("alert-error").addClass("alert-info");
                $("#divErrMsg").slideDown();
                $("#divErrMsg span").html('<img src="assets/img/loader3.gif"/>&nbsp;Saving Tweet response message');
                $.ajax({
                    type: 'POST',
                    url: 'WebService/SelfiesService.asmx/saveResponses',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ responses: responses, evercamId: localStorage.getItem("evercamUserId") }),
                    dataType: 'json',
                    context: $('body'),
                    success: function (json) {
                        $("#divErrMsg span").html('Saved Tweet response message.');
                    },
                    error: function (jqxhr, textstatus, errorthrown) {
                        $("#divErrMsg").removeClass("alert-info").addClass("alert-error");
                        $("#divErrMsg span").html(errorthrown);
                    }
                });
            }
            
        });
        $(".btnCancelTemplate").bind("click", function () {
            $(".template-body").slideUp();
        });
    }

    var reloadForm = function () {
        if (localStorage.getItem("reloadForm")) {
            $.get('NewTwitterResponse.html', function (data) {
                $("#divResponseForm div").html(data);
                getCameras("");
                $("#divResponseForm").slideDown(500);
                handleFileupload();
                //jQuery('.tooltips').tooltip();
                $("#txtHashTag").val(localStorage.getItem("hashTag"));
                $("#txtFooterText").val(localStorage.getItem("footerText"));
                $("#txtTweetResponse").val(localStorage.getItem("tweetResponse"));            
            });
            $("#lnNewTwitterResponse").hide();
            $("#lnNewTwitterResponseCol").show();
            $("#txtSelfiesId").val(0);
            $("#txtLogoFile").val("");
        }        
    }

    var clearLocalStorage = function () {
        localStorage.removeItem("cameraId");
        localStorage.removeItem("hashTag");
        localStorage.removeItem("footerText")
        localStorage.removeItem("tweetResponse");
        localStorage.removeItem("reloadForm");
    }

    var handleFilter = function () {
        $("#btnFilter").bind("click", function () {
            var filter="";
            if ($("#chkLoggedIn").attr("checked"))
                filter += "4,";
            if ($("#chkLoggedOut").attr("checked"))
                filter += "5,";
            if ($("#chkSelfiesCreated").attr("checked"))
                filter += "1,";
            if ($("#chkSelfiesUpdated").attr("checked"))
                filter += "2,";
            if ($("#chkSelfiesDeleted").attr("checked"))
                filter += "3,";
            if ($("#chkTwitterConnect").attr("checked"))
                filter += "6,8,";
            if ($("#chkTwitterSwitch").attr("checked"))
                filter += "7,";
            if ($("#TwitterForget").attr("checked"))
                filter += "9,";
            filter = filter.substring(0, filter.length - 1);
            loadLogs(filter);
        });

        $("#btnClearFilter").bind("click", function () {
            loadLogs("");
            $("#uniform-chkLoggedIn span").addClass("checked");
            $("#chkLoggedIn").attr("checked", "checked");

            $("#uniform-chkLoggedOut span").addClass("checked");
            $("#chkLoggedOut").attr("checked", "checked");

            $("#uniform-chkSelfiesCreated span").addClass("checked");
            $("#chkSelfiesCreated").attr("checked", "checked");

            $("#uniform-chkSelfiesUpdated span").addClass("checked");
            $("#chkSelfiesUpdated").attr("checked", "checked");

            $("#uniform-chkSelfiesDeleted span").addClass("checked");
            $("#chkSelfiesDeleted").attr("checked", "checked");

            $("#uniform-chkTwitterConnect span").addClass("checked");
            $("#chkTwitterConnect").attr("checked", "checked");

            $("#uniform-chkTwitterSwitch span").addClass("checked");
            $("#chkTwitterSwitch").attr("checked", "checked");

            $("#uniform-TwitterForget span").addClass("checked");
            $("#TwitterForget").attr("checked", "checked");

            $("#uniform-chkAll span").addClass("checked");
            $("#chkAll").attr("checked", "checked");
        });

        $(".toggleCheckBox").bind("click", function () {
            if ($("#chkLoggedIn").attr("checked") && $("#chkLoggedOut").attr("checked") && $("#chkSelfiesCreated").attr("checked") && $("#chkSelfiesUpdated").attr("checked") && $("#chkSelfiesDeleted").attr("checked") 
                && $("#chkTwitterConnect").attr("checked") && $("#chkTwitterSwitch").attr("checked") && $("#TwitterForget").attr("checked")) {
                $("#uniform-chkAll span").addClass("checked");
                $("#chkAll").attr("checked", "checked");
            } else {
                $("#uniform-chkAll span").removeClass("checked");
                $("#chkAll").attr("checked", false);
            }
        });
                
        $("#chkAll").bind("click", function () {
            if ($("#chkAll").attr("checked")) {

                $("#uniform-chkLoggedIn span").addClass("checked");
                $("#chkLoggedIn").attr("checked", "checked");

                $("#uniform-chkLoggedOut span").addClass("checked");
                $("#chkLoggedOut").attr("checked", "checked");

                $("#uniform-chkSelfiesCreated span").addClass("checked");
                $("#chkSelfiesCreated").attr("checked", "checked");

                $("#uniform-chkSelfiesUpdated span").addClass("checked");
                $("#chkSelfiesUpdated").attr("checked", "checked");

                $("#uniform-chkSelfiesDeleted span").addClass("checked");
                $("#chkSelfiesDeleted").attr("checked", "checked");

                $("#uniform-chkTwitterConnect span").addClass("checked");
                $("#chkTwitterConnect").attr("checked", "checked");

                $("#uniform-chkTwitterSwitch span").addClass("checked");
                $("#chkTwitterSwitch").attr("checked", "checked");

                $("#uniform-TwitterForget span").addClass("checked");
                $("#TwitterForget").attr("checked", "checked");
            }
            else {
                $("#uniform-chkLoggedIn span").removeClass("checked");
                $("#chkLoggedIn").attr("checked", false);

                $("#uniform-chkLoggedOut span").removeClass("checked");
                $("#chkLoggedOut").attr("checked", false);

                $("#uniform-chkSelfiesCreated span").removeClass("checked");
                $("#chkSelfiesCreated").attr("checked", false);

                $("#uniform-chkSelfiesUpdated span").removeClass("checked");
                $("#chkSelfiesUpdated").attr("checked", false);

                $("#uniform-chkSelfiesDeleted span").removeClass("checked");
                $("#chkSelfiesDeleted").attr("checked", false);

                $("#uniform-chkTwitterConnect span").removeClass("checked");
                $("#chkTwitterConnect").attr("checked", false);

                $("#uniform-chkTwitterSwitch span").removeClass("checked");
                $("#chkTwitterSwitch").attr("checked", false);

                $("#uniform-TwitterForget span").removeClass("checked");
                $("#TwitterForget").attr("checked", false);
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            //if (!handleLogin()) //check login
            //    return;
            handleLoginSection();
            handleLogout(); // Handle logout functionality
            handleWindowRessize();
            handleTabs();
            showResponseForm();
            handleFilter();
        }

    };
}();