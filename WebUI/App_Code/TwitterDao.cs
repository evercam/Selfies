﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Selfies.Entities;
using System.IO;

namespace Selfies.Dao
{
    public class TwitterDao
    {
        public static Twitters GetTwiterById(Twitters tweet)
        {
            Twitters twitter = new Twitters();
            try
            {
                const string sql = @"select * from Twitters where ID=@ID";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@ID", string.IsNullOrEmpty(tweet.ID) ? (object)DBNull.Value : tweet.ID);
                //cmd.Parameters.AddWithValue("@EvercamId", string.IsNullOrEmpty(tweet.EvercamId) ? (object)DBNull.Value : tweet.EvercamId);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (!dr.IsDBNull(dr.GetOrdinal("ID")))
                        twitter.ID = dr["ID"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("AccessToken")))
                        twitter.AccessToken = dr["AccessToken"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("TokenSecret")))
                        twitter.TokenSecret = dr["TokenSecret"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("EvercamId")))
                        twitter.EvercamId = dr["EvercamId"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("IsActive")))
                        twitter.IsActive = dr.GetBoolean(dr.GetOrdinal("IsActive"));
                    
                }
                dr.Close();
                dr.Dispose();
                cmd.Dispose();
                DbConnection.CloseConnection();
                return twitter;
            }
            catch (Exception ex)
            {
                string msg =
                    "TwitterDao GetTwiterById(Twitters tweet) <br /> Log Message: " + ex.Message;
                LogError(msg);
                return twitter;
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static Twitters GetTwiterById(string evercamId)
        {
            Twitters twitter = new Twitters();
            try
            {
                const string sql = @"select * from Twitters where EvercamId=@evercamId";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@evercamId", evercamId);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (!dr.IsDBNull(dr.GetOrdinal("ID")))
                        twitter.ID = dr["ID"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("AccessToken")))
                        twitter.AccessToken = dr["AccessToken"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("TokenSecret")))
                        twitter.TokenSecret = dr["TokenSecret"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("EvercamId")))
                        twitter.EvercamId = dr["EvercamId"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("IsActive")))
                        twitter.IsActive = dr.GetBoolean(dr.GetOrdinal("IsActive"));

                }
                dr.Close();
                dr.Dispose();
                cmd.Dispose();
                DbConnection.CloseConnection();
                return twitter;
            }
            catch (Exception ex)
            {
                string msg =
                    "TwitterDao GetTwiterById(Twitters tweet) <br /> Log Message: " + ex.Message;
                LogError(msg);
                return twitter;
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static List<Twitters> GetTwiterByEvercamId(string evercamId)
        {
            List<Twitters> twitters = new List<Twitters>();
            try
            {
                const string sql = @"select * from Twitters where EvercamId=@EvercamId and IsActive=1";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@EvercamId", string.IsNullOrEmpty(evercamId) ? (object)DBNull.Value : evercamId);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Twitters twitter = new Twitters();
                    if (!dr.IsDBNull(dr.GetOrdinal("ID")))
                        twitter.ID = dr["ID"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("AccessToken")))
                        twitter.AccessToken = dr["AccessToken"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("TokenSecret")))
                        twitter.TokenSecret = dr["TokenSecret"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("EvercamId")))
                        twitter.EvercamId = dr["EvercamId"].ToString();
                    twitters.Add(twitter);

                }
                dr.Close();
                dr.Dispose();
                cmd.Dispose();
                DbConnection.CloseConnection();
                return twitters;
            }
            catch (Exception ex)
            {
                string msg =
                    "TwitterDao GetTwiter(string accessToken, string tokenSecret) <br /> Log Message: " + ex.Message;
                LogError(msg);
                return twitters;
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static void InsertTwitters(Twitters twitter)
        {
            try
            {
                const string sql = @"insert into Twitters(ID,AccessToken, TokenSecret,EvercamId,IsActive) values (@Id,@accessToken, @tokenSecret,@EvercamId,1)";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@Id", string.IsNullOrEmpty(twitter.ID) ? (object)DBNull.Value : twitter.ID);
                cmd.Parameters.AddWithValue("@EvercamId", string.IsNullOrEmpty(twitter.EvercamId) ? (object)DBNull.Value : twitter.EvercamId);
                cmd.Parameters.AddWithValue("@accessToken", string.IsNullOrEmpty(twitter.AccessToken) ? (object)DBNull.Value : twitter.AccessToken);
                cmd.Parameters.AddWithValue("@tokenSecret", string.IsNullOrEmpty(twitter.TokenSecret) ? (object)DBNull.Value : twitter.TokenSecret);
                
                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                DbConnection.CloseConnection();
                //dont remove this line
                //it will updates camera statistics
            }
            catch (Exception ex)
            {
                string msg =
                    "TwitterDao InsertTwitters(Twitters twitter) <br /> Log Message: " + ex.Message;
                LogError(msg);
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static void UpdateTwitterInfo(Twitters twitter)
        {
            try
            {
                const string sql = @"update Twitters set ID=@Id, AccessToken=@accessToken, TokenSecret=@tokenSecret,IsActive=1 where EvercamId=@EvercamId";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@Id", twitter.ID);
                cmd.Parameters.AddWithValue("@EvercamId", string.IsNullOrEmpty(twitter.EvercamId) ? (object)DBNull.Value : twitter.EvercamId);
                cmd.Parameters.AddWithValue("@accessToken", string.IsNullOrEmpty(twitter.AccessToken) ? (object)DBNull.Value : twitter.AccessToken);
                cmd.Parameters.AddWithValue("@tokenSecret", string.IsNullOrEmpty(twitter.TokenSecret) ? (object)DBNull.Value : twitter.TokenSecret);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                DbConnection.CloseConnection();
            }
            catch (Exception ex)
            {
                string msg =
                    "TwitterDao UpdateTwitterInfo(Twitters twitter) <br /> Log Message: " + ex.Message;
                LogError(msg);
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static void ActiveInactiveTwitter(string twitterId, string evercamId, bool isActive)
        {
            try
            {
                const string sql = @"update Twitters set IsActive=@IsActive where ID=@Id and EvercamId=@EvercamId";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@Id", twitterId);
                cmd.Parameters.AddWithValue("@EvercamId", string.IsNullOrEmpty(evercamId) ? (object)DBNull.Value : evercamId);
                cmd.Parameters.AddWithValue("@IsActive", isActive);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                DbConnection.CloseConnection();
            }
            catch (Exception ex)
            {
                string msg =
                    "TwitterDao ActiveInactiveTwitter(string twitterId, string evercamId, bool isActive) <br /> Log Message: " + ex.Message;
                LogError(msg);
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static void InactiveTwitterByEvercamId(string evercamId)
        {
            try
            {
                const string sql = @"update Twitters set IsActive=0 where EvercamId=@EvercamId";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@EvercamId", string.IsNullOrEmpty(evercamId) ? (object)DBNull.Value : evercamId);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                DbConnection.CloseConnection();
            }
            catch (Exception ex)
            {
                string msg =
                    "TwitterDao InactiveTwitterByEvercamId(string evercamId) <br /> Log Message: " + ex.Message;
                LogError(msg);
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static void DeleteTwitterAccount(string twitterId)
        {
            try
            {
                const string sql = @"delete from Twitters where ID=@twitterId";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@twitterId", twitterId);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                DbConnection.CloseConnection();
            }
            catch (Exception ex)
            {
                string msg =
                    "TwitterDao DeleteTwitterAccount(string twitterId) <br /> Log Message: " + ex.Message;
                LogError(msg);
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static void LogError(string message)
        {
            try
            {
                var fileName = @"C:\selfiesdemo\logs\TwitterDao.txt";
                StreamWriter file = new StreamWriter(fileName, true);
                file.WriteLine(message + "\t" + DateTime.UtcNow + "\n");
                file.Close();
            }
            catch (Exception) { }

        }
    }

}