﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Entities
{
    public class Selfies
    {
        public int ID { get; set; }
        public string CameraID { get; set; }
        public string TwitterID { get; set; }
        public string EvercamID { get; set; }
        public string AccessToken { get; set; }
        public string HashTag { get; set; }
        public string PhotoText { get; set; }
        public string SnapshotUrl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string LogoFile { get; set; }
        public string Response { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
