﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Selfies.Entities
{
    public class Twitters
    {
        public string ID { get; set; }
        public string EvercamId { get; set; }
        public string AccessToken { get; set; }
        public string TokenSecret { get; set; }
        public bool IsActive { get; set; }
    }
}