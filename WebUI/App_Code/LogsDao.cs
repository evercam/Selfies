﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Selfies.Entities;
using System.IO;

namespace Selfies.Dao
{
    public class LogsDao
    {
        public static List<Logs> GetLogsByEvercamId(string evercamId, string filter)
        {
            List<Logs> logs = new List<Logs>();
            try
            {
                string sql = @"select * from Logs where CreatedBy=@evercamId ";
                if (!string.IsNullOrEmpty(filter))
                    sql += " and Type in (" + filter + ")";
                sql += " order by ID desc";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@evercamId", string.IsNullOrEmpty(evercamId) ? (object)DBNull.Value : evercamId);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Logs log = new Logs();
                    if (!dr.IsDBNull(dr.GetOrdinal("ID")))
                        log.ID = dr.GetInt32(dr.GetOrdinal("ID"));
                    if (!dr.IsDBNull(dr.GetOrdinal("CreatedBy")))
                        log.CreatedBy = dr["CreatedBy"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("SelfiesId")))
                        log.SelfiesId = dr.GetInt32(dr.GetOrdinal("SelfiesId"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Tweet")))
                        log.Tweet = dr["Tweet"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("Message")))
                        log.Message = dr["Message"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("TwitterId")))
                        log.TwitterId = dr["TwitterId"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("IP")))
                        log.IP = dr["IP"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("Type")))
                        log.Type = (ActionType)dr.GetInt32(dr.GetOrdinal("Type"));
                    if (!dr.IsDBNull(dr.GetOrdinal("CreatedAt")))
                    {
                        log.CreatedAt = dr.GetDateTime(dr.GetOrdinal("CreatedAt"));
                        log.FormatedDate = log.CreatedAt.ToString("dd/MM/yyyy hh:mm:ss");
                    }
                    logs.Add(log);
                }
                dr.Close();
                dr.Dispose();
                cmd.Dispose();
                DbConnection.CloseConnection();
                return logs;
            }
            catch (Exception ex)
            {
                string msg =
                    "LogsDao GetLogsByEvercamId(string evercamId, string filter) <br /> Log Message: " + ex.Message;
                LogError(msg);
                return logs;
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static int InsertLog(string evercamId, string twitterId, string logMessage, string ip, ActionType actionType)
        {
            int logId = 0;
            try
            {
                const string sql = @"insert into Logs(CreatedBy,TwitterId, Message, IP,Type) 
                                    values (@evercamId, @twitterId, @logMessage, @ip,@Type) 
                                    SELECT ID FROM Selfies WHERE ID=Scope_Identity()";
                SqlCommand cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.AddWithValue("@evercamId", string.IsNullOrEmpty(evercamId) ? (object)DBNull.Value : evercamId);
                cmd.Parameters.AddWithValue("@twitterId", string.IsNullOrEmpty(twitterId) ? (object)DBNull.Value : twitterId);
                cmd.Parameters.AddWithValue("@logMessage", string.IsNullOrEmpty(logMessage) ? (object)DBNull.Value : logMessage);
                cmd.Parameters.AddWithValue("@ip", string.IsNullOrEmpty(ip) ? (object)DBNull.Value : ip);
                cmd.Parameters.AddWithValue("@Type", actionType);

                DbConnection.OpenConnection();
                cmd.Connection = DbConnection.Connection;
                SqlDataReader dr = cmd.ExecuteReader();//try to update the object
                while (dr.Read())
                {
                    if (!dr.IsDBNull(dr.GetOrdinal("ID")))
                        logId = dr.GetInt32(dr.GetOrdinal("ID"));
                }
                dr.Close();
                dr.Dispose();
                cmd.Dispose();
                DbConnection.CloseConnection();
                return logId;
            }
            catch (Exception ex)
            {
                string msg =
                    "LogsDao InsertLog(string evercamId, string twitterId, string logMessage, string ip, ActionType actionType) <br /> Log Message: " + ex.Message;
                LogError(msg);
                return logId;
            }
            finally
            { DbConnection.CloseConnection(); }
        }

        public static void LogError(string message)
        {
            try
            {
                var fileName = @"C:\selfiesdemo\logs\LogsDao.txt";
                StreamWriter file = new StreamWriter(fileName, true);
                file.WriteLine(message + "\t" + DateTime.UtcNow + "\n");
                file.Close();
            }
            catch (Exception) { }

        }
    }
}