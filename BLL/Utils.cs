﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Text;
using System.IO;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace BLL
{
    public enum LogTypes
    {
        Info = 100,
        Debug = 101,
        Error = 102,
    }

    public class Utils
    {
        public static DateTime SQLMinDate = new DateTime(1900, 1, 1, 12, 0, 0);     // changed from 1753    to fix utc
        public static DateTime SQLMaxDate = new DateTime(8888, 12, 31, 23, 59, 59); // changed from 9999    conversion errors

        public static string SiteUrl = ConfigurationSettings.AppSettings["SiteUrl"];
        public static string LogoPathPrefix = ConfigurationSettings.AppSettings["LogoPathPrefix"];
        public static string ListenerPath = ConfigurationSettings.AppSettings["ListenerPath"];
        public static string ListenerProcess = ConfigurationSettings.AppSettings["ListenerProcess"];
        public static string SynchInterval = ConfigurationSettings.AppSettings["SynchInterval"];

        public static string AppName = ConfigurationSettings.AppSettings["TwitterAppName"];
        public static string ConsumerKey = ConfigurationSettings.AppSettings["TwitterConsumerKey"];
        public static string ConsumerSecret = ConfigurationSettings.AppSettings["TwitterConsumerSecret"];
        public static string AccessToken = ConfigurationSettings.AppSettings["TwitterToken"];
        public static string AccessTokenSecret = ConfigurationSettings.AppSettings["TwitterTokenSecret"];

        public static string ServerName = ConfigurationSettings.AppSettings["ServerName"];
        public static string ServerPipeName = ConfigurationSettings.AppSettings["ServerPipeName"];

        public static string HashFileWatchPath = ConfigurationSettings.AppSettings["HashFileWatchPath"];
        public static string TwitterFileWatchPath = ConfigurationSettings.AppSettings["TwitterFileWatchPath"];

        public static string CameraHashTag = ConfigurationSettings.AppSettings["CameraHashTag"];
        public static string CameraUrl = ConfigurationSettings.AppSettings["CameraUrl"];
        public static string CameraUsername = ConfigurationSettings.AppSettings["CameraUsername"];
        public static string CameraPassword = ConfigurationSettings.AppSettings["CameraPassword"];
        public static string CameraPhotoTag = ConfigurationSettings.AppSettings["CameraPhotoTag"];

        public static string TweetMessageTemplate = ConfigurationSettings.AppSettings["TweetMessageTemplate"];
        public static string TweetWithoutImage = ConfigurationSettings.AppSettings["TweetWithoutImage"];
        public static string SnapshotCacheLife = ConfigurationSettings.AppSettings["SnapshotCacheLife"];
        public static string WriteImagesToDir = ConfigurationSettings.AppSettings["WriteImagesToDir"];

        public static string WatermarkFontName = ConfigurationSettings.AppSettings["WatermarkFontName"];
        public static string WatermarkFontSize = ConfigurationSettings.AppSettings["WatermarkFontSize"];
        public static string WatermarkFontColor = ConfigurationSettings.AppSettings["WatermarkFontColor"];
        public static string WatermarkFontOutline = ConfigurationSettings.AppSettings["WatermarkFontOutline"];
        public static string WatermarkTextHzAlign = ConfigurationSettings.AppSettings["WatermarkTextHzAlign"];
        public static string WatermarkTextVtAlign = ConfigurationSettings.AppSettings["WatermarkTextVtAlign"];
        public static string WatermarkMargin = ConfigurationSettings.AppSettings["WatermarkMargin"];

        public static string DeveloperEmail = ConfigurationSettings.AppSettings["DeveloperEmail"];

        public void AddWaterMark(string source, string watermark)
        {
            try
            {
                Image image = System.Drawing.Image.FromFile(source);    //This is the background image 
                Image logo = Image.FromFile(watermark);                 //This is your watermark 
                Graphics g = System.Drawing.Graphics.FromImage(image);  //Create graphics object of the background image //So that you can draw your logo on it
                Bitmap TransparentLogo = new Bitmap(logo.Width, logo.Height);   //Create a blank bitmap object //to which we //draw our transparent logo
                Graphics TGraphics = Graphics.FromImage(TransparentLogo);       //Create a graphics object so that //we can draw //on the blank bitmap image object
                ColorMatrix ColorMatrix = new ColorMatrix();            //An image is represenred as a 5X4 matrix(i.e 4 //columns and 5 //rows) 
                ColorMatrix.Matrix33 = 0.25F;                           //the 3rd element of the 4th row represents the transparency 
                ImageAttributes ImgAttributes = new ImageAttributes();  //an ImageAttributes object is used to set all //the alpha //values.This is done by initializing a color matrix and setting the alpha scaling value in the matrix.The address of //the color matrix is passed to the SetColorMatrix method of the //ImageAttributes object, and the //ImageAttributes object is passed to the DrawImage method of the Graphics object.
                ImgAttributes.SetColorMatrix(ColorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
                TGraphics.DrawImage(logo, new Rectangle(0, 0, TransparentLogo.Width, TransparentLogo.Height), 0, 0, TransparentLogo.Width, TransparentLogo.Height, GraphicsUnit.Pixel, ImgAttributes);
                TGraphics.Dispose();
                g.DrawImage(TransparentLogo, image.Width - TransparentLogo.Width, 0);
                image.Save(@"e:\testing.jpeg", ImageFormat.Jpeg);
            }
            catch (Exception x) { Console.WriteLine("Error in Utils.AddWaterMark: " + x.ToString()); }
        }

        public static string ListenerName(BLL.Entities.Twitter twitter)
        {
            return "listener_" + twitter.ID;
        }

        public static string SelfiesKey(BLL.Entities.Selfies self)
        {
            return SelfiesKey((string.IsNullOrEmpty(self.TwitterID) ? "" : self.TwitterID), (string.IsNullOrEmpty(self.HashTag) ? "" : self.HashTag));
        }

        public static string SelfiesKey(string tid, string tag)
        {
            return tid.ToLower() + tag.ToLower();
        }

        public static string ProcessKey(BLL.Entities.Twitter twitter)
        {
            return ProcessKey(twitter.ID);
        }

        public static string ProcessKey(string tid)
        {
            return tid.ToLower();
        }

        public static void FileLog(string msg)
        {
            try
            {
                System.IO.Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
                string logPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "logs");

                if (!Directory.Exists(logPath))
                    Directory.CreateDirectory(logPath);

                string logFile = Path.Combine(logPath, DateTime.UtcNow.ToString("yyyy-MM-dd-HH") + ".txt");

                StreamWriter file = new StreamWriter(logFile, true);
                file.WriteLine(DateTime.UtcNow + "\t" + msg + "\n");
                file.Close();
            }
            catch (Exception x) { Console.WriteLine("Error in Utils.FileLog: " + x.Message); }
        }

        public static void DBLog(Entities.Log log)
        {
            try
            {
                DAO.LogsDao.Insert(log);
            }
            catch (Exception x) { Console.WriteLine("Error in Utils.DbLog: " + x.Message); }
        }

        public static void DBLog(LogTypes type, string message, string createdBy)
        {
            try
            {
                DAO.LogsDao.Insert(new Entities.Log { Message = message, SelfiesId = 0, Tweet = "", TwitterId = "", Type = (int)type, IP = "", CreatedBy = createdBy, CreatedAt = DateTime.Now });
            }
            catch (Exception x) { Console.WriteLine("Error in Utils.DbLog: " + x.Message); }
        }

        public static void DBLog(LogTypes type, string message, string twitterId, string createdBy)
        {
            try
            {
                DAO.LogsDao.Insert(new Entities.Log { Message = message, SelfiesId = 0, Tweet = "", TwitterId = twitterId, Type = (int)type, IP = "", CreatedBy = createdBy, CreatedAt = DateTime.Now });
            }
            catch (Exception x) { Console.WriteLine("Error in Utils.DbLog: " + x.Message); }
        }

        public static void DBLog(LogTypes type, string message, int selfiesId, string tweet, string twitterId, string createdBy)
        {
            try
            {
                DAO.LogsDao.Insert(new Entities.Log { Message = message, SelfiesId = selfiesId, Tweet = tweet, TwitterId = twitterId, Type = (int)type, IP = "", CreatedBy = createdBy, CreatedAt = DateTime.Now });
            }
            catch (Exception x) { Console.WriteLine("Error in Utils.DbLog: " + x.Message); }
        }

        public static bool SaveFile(string fileName, byte[] data)
        {
            try
            {
                string path = fileName.Substring(0, fileName.LastIndexOf(@"\"));

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                FileStream stream = new FileStream(fileName, FileMode.Create);
                stream.Write(data, 0, data.Length);
                stream.Close();
                stream.Dispose();
                return true;
            }
            catch (Exception x)
            {
                return false;
            }
        }

        public static MemoryStream DownloadImageStream(string url, string username, string password)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.KeepAlive = false;

                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                    request.Credentials = new NetworkCredential(username, password);

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (MemoryStream ms = new MemoryStream(60000))
                        {
                            if (response.ContentType.Contains("image") && stream != null)
                            {
                                return ms;
                            }
                        }
                    }
                }
            }
            catch (Exception x) { Console.WriteLine("Error in Utils.DownloadImageStream: " + x.Message); }
            return new MemoryStream();
        }

        public static byte[] DownloadImageBytes(string url, string username, string password)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.KeepAlive = false;

            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                request.Credentials = new NetworkCredential(username, password);
            
            byte[] bytes = new byte[] {};

            try
            {
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (MemoryStream ms = new MemoryStream(60000))
                        {
                            if (response.ContentType.Contains("image") && stream != null)
                            {
                                stream.CopyTo(ms);
                                bytes = ms.ToArray();
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                Console.WriteLine("Error Utils.DownloadImageBytes: " + x.Message + Environment.NewLine + x.InnerException.ToString());
            }

            return bytes;
        }

        public static byte[] WatermarkImage(byte[] imagedata, string titletext, string logofile, string hashtag)
        {
            try
            {
                MemoryStream stream = new MemoryStream(imagedata);
                Image image = System.Drawing.Image.FromStream(stream);
                Graphics g = System.Drawing.Graphics.FromImage(image);

                if (!string.IsNullOrEmpty(titletext))
                    AddText(titletext, g, image.Width, image.Height);

                if (!string.IsNullOrEmpty(logofile))
                    AddLogo(g, logofile, image.Width);

                byte[] bytes = new byte[] { };
                using (MemoryStream ms = new MemoryStream())
                {
                    if (!string.IsNullOrEmpty(WriteImagesToDir))
                    {
                        if (File.Exists(Path.Combine(WriteImagesToDir, hashtag.Replace("#", "-") + ".jpg")))
                            File.Delete(Path.Combine(WriteImagesToDir, hashtag.Replace("#", "-") + ".jpg"));
                        image.Save(Path.Combine(WriteImagesToDir, hashtag.Replace("#", "-") + ".jpg"), ImageFormat.Jpeg);
                    }
                    image.Save(ms, ImageFormat.Jpeg);
                    bytes = ms.ToArray();
                }

                stream.Close();
                stream.Dispose();
                return bytes;
            }
            catch (Exception x)
            {
                Console.WriteLine("Error Utils.WatermarkImage() " + x.Message + Environment.NewLine + x.InnerException.ToString());
                return new byte[] {};
            }
        }

        private static void AddText(string text, Graphics g, int width, int height)
        {
            try
            {
                float fontSize = float.Parse(WatermarkFontSize);

                StringFormat sf = new StringFormat();

                if (WatermarkTextHzAlign.ToLower() == "left")
                    sf.Alignment = StringAlignment.Near;
                else if (WatermarkTextHzAlign.ToLower() == "center")
                    sf.Alignment = StringAlignment.Center;
                else if (WatermarkTextHzAlign.ToLower() == "right")
                    sf.Alignment = StringAlignment.Far;

                if (WatermarkTextVtAlign.ToLower() == "top")
                    sf.LineAlignment = StringAlignment.Near;
                else if (WatermarkTextVtAlign.ToLower() == "middle")
                    sf.LineAlignment = StringAlignment.Center;
                else if (WatermarkTextVtAlign.ToLower() == "bottom")
                    sf.LineAlignment = StringAlignment.Far;

                Font f = new Font(WatermarkFontName, fontSize, FontStyle.Bold, GraphicsUnit.Pixel);

                Pen p = new Pen(ColorTranslator.FromHtml(WatermarkFontOutline), 5);
                p.LineJoin = LineJoin.Round;

                Rectangle fr = new Rectangle(0, height - f.Height, width, f.Height);
                LinearGradientBrush b = new LinearGradientBrush(
                    fr, ColorTranslator.FromHtml(WatermarkFontColor), ColorTranslator.FromHtml(WatermarkFontColor), 90);

                Rectangle r = new Rectangle(0, 0, width, height);

                GraphicsPath gp = new GraphicsPath();

                gp.AddString(text, f.FontFamily, (int)f.Style, fontSize, r, sf);

                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;

                //TODO: shadow -> g.translate, fillpath once, remove translate
                g.DrawPath(p, gp);
                g.FillPath(b, gp);

                //cleanup
                gp.Dispose();
                b.Dispose();
                b.Dispose();
                f.Dispose();
                sf.Dispose();
            }
            catch (Exception x) { Console.WriteLine("Error Utils.AddText: " + x.Message + Environment.NewLine + x.InnerException.ToString()); }
        }

        private static void AddLogo(Graphics g, string watermarkFile, int width)
        {
            try
            {
                string path = WebUtility.UrlDecode(watermarkFile);
                path = path.Replace(Utils.SiteUrl, Utils.LogoPathPrefix).Replace(@"/", @"\\");
                if (!File.Exists(path))
                {
                    Console.WriteLine("Not Found: " + path);
                    return;
                }

                Image logo = Image.FromFile(path);
                Bitmap TransparentLogo = new Bitmap(logo.Width, logo.Height);
                Graphics TGraphics = Graphics.FromImage(TransparentLogo);
                ColorMatrix ColorMatrix = new ColorMatrix();
                ColorMatrix.Matrix33 = 0.50F;           // transparency
                ImageAttributes ImgAttributes = new ImageAttributes();
                ImgAttributes.SetColorMatrix(ColorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
                TGraphics.DrawImage(logo, new Rectangle(0, 0, TransparentLogo.Width, TransparentLogo.Height), 0, 0, TransparentLogo.Width, TransparentLogo.Height, GraphicsUnit.Pixel, ImgAttributes);
                TGraphics.Dispose();

                g.DrawImage(TransparentLogo, width - (TransparentLogo.Width + int.Parse(Utils.WatermarkMargin)), int.Parse(Utils.WatermarkMargin));
            }
            catch (Exception x) { Console.WriteLine("Error Utils.AddLogo: " + x.Message + Environment.NewLine + x.ToString()); }
        }

        private static byte[] AppendFooter(Image snapshot, string footerimage, string tag)
        {
            Image footer = Image.FromFile(footerimage);
            List<Image> images = new List<Image>() { snapshot, footer };
            System.Drawing.Bitmap finalImage = null;

            try
            {
                int width = 0;
                int height = 0;

                foreach (Image image in images)
                {
                    height += image.Height;
                    width = image.Width > width ? image.Width : width;
                }

                finalImage = new System.Drawing.Bitmap(width, height);

                using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(finalImage))
                {
                    g.Clear(System.Drawing.Color.White);

                    int offsetx = 0;
                    int offsety = 0;
                    foreach (System.Drawing.Image image in images)
                    {
                        offsetx = (finalImage.Width - image.Width) / 2;
                        g.DrawImage(image, new System.Drawing.Rectangle(offsetx, offsety, image.Width, image.Height));
                        offsety += image.Height;
                    }
                }

                byte[] bytes = new byte[] {};
                using (MemoryStream ms = new MemoryStream()) {
                    if (!string.IsNullOrEmpty(WriteImagesToDir))
                    {
                        if (File.Exists(Path.Combine(WriteImagesToDir, tag.Trim('#') + ".jpg")))
                            File.Delete(Path.Combine(WriteImagesToDir, tag.Trim('#') + ".jpg"));
                        finalImage.Save(Path.Combine(WriteImagesToDir, tag.Trim('#') + ".jpg"), ImageFormat.Jpeg);
                    }
                    finalImage.Save(ms, ImageFormat.Jpeg);
                    bytes = ms.ToArray();
                }
                
                return bytes;
            }
            catch (Exception x)
            {
                if (finalImage != null)
                    finalImage.Dispose();

                Console.WriteLine("Error in Utils.AppendFooter() " + x.Message);
            }
            finally
            {
                foreach (Image image in images)
                    image.Dispose();
            }
            return new byte[] { };
        }

        public static string EncodeUrl(string url)
        {
            for (int i = 0; i < _chars.GetUpperBound(0); i++)
                url = url.Replace(_chars[i, 0], _chars[i, 1]);

            return url;
        }

        public static string DecodeUrl(string url)
        {
            for (int i = 0; i < _chars.GetUpperBound(0); i++)
                url = url.Replace(_chars[i, 1], _chars[i, 0]);

            return url;
        }

        public static string ShortenUrl(string url)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch
                (url, @"(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?"))
                throw new FormatException("The URL you specificed is not in the current format.");

            url = Uri.EscapeUriString(url);
            string reqUri = String.Format(@"http://is.gd/api.php?longurl={0}", url);

            WebRequest req = WebRequest.Create(reqUri);
            req.Timeout = 5000;
            using (System.IO.StreamReader reader =
                new System.IO.StreamReader(req.GetResponse().GetResponseStream()))
            {
                return reader.ReadLine();
            }
        }

        public static List<string> HashTags(string text)
        {
            string[] words = Regex.Split(text, "(\\#\\w+)");
            List<string> tags = new List<string>();
            foreach (string s in words)
            {
                if (s.StartsWith("#"))
                    tags.Add(s);
            }
            return tags;
        }

        public static string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"[{0}]+", invalidChars);
            string replace = Regex.Replace(name, invalidReStr, "_").Replace(";", "").Replace(",", "");
            return replace;
        }

        public static void SendMail(string subject, string message, string recipients)
        {
            SmtpClient smtp = new SmtpClient
            {
                Host = ConfigurationSettings.AppSettings["SmtpServer"],
                Port = int.Parse(ConfigurationSettings.AppSettings["SmtpServerPort"]),
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(ConfigurationSettings.AppSettings["SmtpUser"], ConfigurationSettings.AppSettings["SmtpPassword"])
            };

            MailMessage mail = new MailMessage
            {
                From = new MailAddress(ConfigurationSettings.AppSettings["SmtpEmail"], ConfigurationSettings.AppSettings["EmailSource"]),
                Subject = subject,
                Body = message,
                IsBodyHtml = true
            };

            try
            {
                string[] emails = recipients.Split(new char[] { ';', ',', ' ' });
                foreach (string email in emails)
                {
                    //mail.To.Clear();
                    if (!string.IsNullOrEmpty(email))
                        mail.To.Add(new MailAddress(email));
                }
                if (mail.To.Count > 0)
                    smtp.Send(mail);
            }
            catch (Exception x)
            {

            }
        }

        private static string[,] _chars = new string[,]
        {
        { "%", "%25" },     // this is the first one
        { "$" , "%24" },
        { "&", "%26" },
        { "+", "%2B" },
        { ",", "%2C" },
        { "/", "%2F" },
        { ":", "%3A" },
        { ";", "%3B" },
        { "=", "%3D" },
        { "?", "%3F" },
        { "@", "%40" },
        { " ", "%20" },
        { "\"" , "%22" },
        { "<", "%3C" },
        { ">", "%3E" },
        { "#", "%23" },
        { "{", "%7B" },
        { "}", "%7D" },
        { "|", "%7C" },
        { "\\", "%5C" },
        { "^", "%5E" },
        { "~", "%7E" },
        { "[", "%5B" },
        { "]", "%5D" },
        { "`", "%60" } };
    }
}
