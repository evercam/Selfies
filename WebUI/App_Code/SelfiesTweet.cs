﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Selfies.Entities
{
    public class SelfiesTweet
    {
        public int Id { get; set; }
        public string TwitterId { get; set; }
        public string EvercamId { get; set; }
        public string CameraId { get; set; }
        public string PhotoText { get; set; }
        public string HashTag { get; set; }
        public string SnapshotUrl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool IsActive { get; set; }
        public string LogoFile { get; set; }
        public bool IsDeleted { get; set; }
        public string Response { get; set; }
        public string AccessToken { get; set; }
    }
}